import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { TagihanService } from 'app/entities/tagihan/tagihan.service';
import { ITagihan, Tagihan } from 'app/shared/model/tagihan.model';
import { StatusBayar } from 'app/shared/model/enumerations/status-bayar.model';

describe('Service Tests', () => {
  describe('Tagihan Service', () => {
    let injector: TestBed;
    let service: TagihanService;
    let httpMock: HttpTestingController;
    let elemDefault: ITagihan;
    let expectedResult: ITagihan | ITagihan[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(TagihanService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Tagihan(0, 'AAAAAAA', currentDate, 0, currentDate, currentDate, StatusBayar.LUNAS);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            jatuhTempo: currentDate.format(DATE_TIME_FORMAT),
            tanggalBayar: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Tagihan', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            jatuhTempo: currentDate.format(DATE_TIME_FORMAT),
            tanggalBayar: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            tanggal: currentDate,
            jatuhTempo: currentDate,
            tanggalBayar: currentDate
          },
          returnedFromService
        );
        service
          .create(new Tagihan())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Tagihan', () => {
        const returnedFromService = Object.assign(
          {
            nomor: 'BBBBBB',
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            total: 1,
            jatuhTempo: currentDate.format(DATE_TIME_FORMAT),
            tanggalBayar: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            tanggal: currentDate,
            jatuhTempo: currentDate,
            tanggalBayar: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Tagihan', () => {
        const returnedFromService = Object.assign(
          {
            nomor: 'BBBBBB',
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            total: 1,
            jatuhTempo: currentDate.format(DATE_TIME_FORMAT),
            tanggalBayar: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            tanggal: currentDate,
            jatuhTempo: currentDate,
            tanggalBayar: currentDate
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Tagihan', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
