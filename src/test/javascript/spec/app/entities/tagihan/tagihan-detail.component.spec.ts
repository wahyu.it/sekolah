import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { TagihanDetailComponent } from 'app/entities/tagihan/tagihan-detail.component';
import { Tagihan } from 'app/shared/model/tagihan.model';

describe('Component Tests', () => {
  describe('Tagihan Management Detail Component', () => {
    let comp: TagihanDetailComponent;
    let fixture: ComponentFixture<TagihanDetailComponent>;
    const route = ({ data: of({ tagihan: new Tagihan(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [TagihanDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TagihanDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TagihanDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tagihan on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tagihan).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
