import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { TagihanUpdateComponent } from 'app/entities/tagihan/tagihan-update.component';
import { TagihanService } from 'app/entities/tagihan/tagihan.service';
import { Tagihan } from 'app/shared/model/tagihan.model';

describe('Component Tests', () => {
  describe('Tagihan Management Update Component', () => {
    let comp: TagihanUpdateComponent;
    let fixture: ComponentFixture<TagihanUpdateComponent>;
    let service: TagihanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [TagihanUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TagihanUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TagihanUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TagihanService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Tagihan(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Tagihan();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
