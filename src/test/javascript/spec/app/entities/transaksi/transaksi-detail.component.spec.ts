import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { TransaksiDetailComponent } from 'app/entities/transaksi/transaksi-detail.component';
import { Transaksi } from 'app/shared/model/transaksi.model';

describe('Component Tests', () => {
  describe('Transaksi Management Detail Component', () => {
    let comp: TransaksiDetailComponent;
    let fixture: ComponentFixture<TransaksiDetailComponent>;
    const route = ({ data: of({ transaksi: new Transaksi(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [TransaksiDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TransaksiDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TransaksiDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load transaksi on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.transaksi).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
