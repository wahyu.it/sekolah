import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { TransaksiUpdateComponent } from 'app/entities/transaksi/transaksi-update.component';
import { TransaksiService } from 'app/entities/transaksi/transaksi.service';
import { Transaksi } from 'app/shared/model/transaksi.model';

describe('Component Tests', () => {
  describe('Transaksi Management Update Component', () => {
    let comp: TransaksiUpdateComponent;
    let fixture: ComponentFixture<TransaksiUpdateComponent>;
    let service: TransaksiService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [TransaksiUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TransaksiUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TransaksiUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TransaksiService);
    });

    /*describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Transaksi(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Transaksi();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);*/
    /* }));
    });*/
  });
});
