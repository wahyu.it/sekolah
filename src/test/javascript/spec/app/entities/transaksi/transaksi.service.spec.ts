import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { TransaksiService } from 'app/entities/transaksi/transaksi.service';
import { ITransaksi, Transaksi } from 'app/shared/model/transaksi.model';
import { MetodeTransaksi } from 'app/shared/model/enumerations/metode-transaksi.model';
import { Bank } from 'app/shared/model/enumerations/bank.model';
import { StatusTransaksi } from 'app/shared/model/enumerations/status-transaksi.model';

describe('Service Tests', () => {
  describe('Transaksi Service', () => {
    let injector: TestBed;
    let service: TransaksiService;
    let httpMock: HttpTestingController;
    let elemDefault: ITransaksi;
    let expectedResult: ITransaksi | ITransaksi[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(TransaksiService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Transaksi(
        0,
        'AAAAAAA',
        currentDate,
        MetodeTransaksi.TUNAI,
        Bank.VA_BCA,
        'AAAAAAA',
        'AAAAAAA',
        StatusTransaksi.SUKSES,
        'AAAAAAA',
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            updateOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        /*service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
*/
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Transaksi', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            updateOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            tanggal: currentDate,
            updateOn: currentDate
          },
          returnedFromService
        );
        /*service
          .create(new Transaksi())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));*/
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Transaksi', () => {
        const returnedFromService = Object.assign(
          {
            nomor: 'BBBBBB',
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            metode: 'BBBBBB',
            bank: 'BBBBBB',
            nomorRekening: 'BBBBBB',
            virtualAccount: 'BBBBBB',
            status: 'BBBBBB',
            updateBy: 'BBBBBB',
            updateOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            tanggal: currentDate,
            updateOn: currentDate
          },
          returnedFromService
        );
        service;
        /* .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));*/
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Transaksi', () => {
        const returnedFromService = Object.assign(
          {
            nomor: 'BBBBBB',
            tanggal: currentDate.format(DATE_TIME_FORMAT),
            metode: 'BBBBBB',
            bank: 'BBBBBB',
            nomorRekening: 'BBBBBB',
            virtualAccount: 'BBBBBB',
            status: 'BBBBBB',
            updateBy: 'BBBBBB',
            updateOn: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            tanggal: currentDate,
            updateOn: currentDate
          },
          returnedFromService
        );
        service.query().pipe(
          take(1),
          map(resp => resp.body)
        );

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Transaksi', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
