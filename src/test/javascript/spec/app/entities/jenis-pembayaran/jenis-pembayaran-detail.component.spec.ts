import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { JenisPembayaranDetailComponent } from 'app/entities/jenis-pembayaran/jenis-pembayaran-detail.component';
import { JenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';

describe('Component Tests', () => {
  describe('JenisPembayaran Management Detail Component', () => {
    let comp: JenisPembayaranDetailComponent;
    let fixture: ComponentFixture<JenisPembayaranDetailComponent>;
    const route = ({ data: of({ jenisPembayaran: new JenisPembayaran(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [JenisPembayaranDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(JenisPembayaranDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JenisPembayaranDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load jenisPembayaran on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.jenisPembayaran).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
