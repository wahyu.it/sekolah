import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { JenisPembayaranUpdateComponent } from 'app/entities/jenis-pembayaran/jenis-pembayaran-update.component';
import { JenisPembayaranService } from 'app/entities/jenis-pembayaran/jenis-pembayaran.service';
import { JenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';

describe('Component Tests', () => {
  describe('JenisPembayaran Management Update Component', () => {
    let comp: JenisPembayaranUpdateComponent;
    let fixture: ComponentFixture<JenisPembayaranUpdateComponent>;
    let service: JenisPembayaranService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [JenisPembayaranUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(JenisPembayaranUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JenisPembayaranUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JenisPembayaranService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new JenisPembayaran(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new JenisPembayaran();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
