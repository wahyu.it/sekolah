import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { BesarBayarUpdateComponent } from 'app/entities/besar-bayar/besar-bayar-update.component';
import { BesarBayarService } from 'app/entities/besar-bayar/besar-bayar.service';
import { BesarBayar } from 'app/shared/model/besar-bayar.model';

describe('Component Tests', () => {
  describe('BesarBayar Management Update Component', () => {
    let comp: BesarBayarUpdateComponent;
    let fixture: ComponentFixture<BesarBayarUpdateComponent>;
    let service: BesarBayarService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [BesarBayarUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BesarBayarUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BesarBayarUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BesarBayarService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BesarBayar(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BesarBayar();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
