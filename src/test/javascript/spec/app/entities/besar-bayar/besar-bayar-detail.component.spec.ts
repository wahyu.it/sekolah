import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { BesarBayarDetailComponent } from 'app/entities/besar-bayar/besar-bayar-detail.component';
import { BesarBayar } from 'app/shared/model/besar-bayar.model';

describe('Component Tests', () => {
  describe('BesarBayar Management Detail Component', () => {
    let comp: BesarBayarDetailComponent;
    let fixture: ComponentFixture<BesarBayarDetailComponent>;
    const route = ({ data: of({ besarBayar: new BesarBayar(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [BesarBayarDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BesarBayarDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BesarBayarDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load besarBayar on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.besarBayar).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
