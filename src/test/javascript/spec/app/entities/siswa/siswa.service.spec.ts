import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { SiswaService } from 'app/entities/siswa/siswa.service';
import { ISiswa, Siswa } from 'app/shared/model/siswa.model';
import { JenisKelamin } from 'app/shared/model/enumerations/jenis-kelamin.model';

describe('Service Tests', () => {
  describe('Siswa Service', () => {
    let injector: TestBed;
    let service: SiswaService;
    let httpMock: HttpTestingController;
    let elemDefault: ISiswa;
    let expectedResult: ISiswa | ISiswa[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(SiswaService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Siswa(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        JenisKelamin.LAKILAKI,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            tanggalLahir: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Siswa', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            tanggalLahir: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            tanggalLahir: currentDate
          },
          returnedFromService
        );
        service
          .create(new Siswa())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Siswa', () => {
        const returnedFromService = Object.assign(
          {
            nis: 'BBBBBB',
            nama: 'BBBBBB',
            alamat: 'BBBBBB',
            jenisKelamin: 'BBBBBB',
            tempatLahir: 'BBBBBB',
            tanggalLahir: currentDate.format(DATE_TIME_FORMAT),
            waliMurid: 'BBBBBB',
            noTelephone: 'BBBBBB',
            tahunAjaran: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            tanggalLahir: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Siswa', () => {
        const returnedFromService = Object.assign(
          {
            nis: 'BBBBBB',
            nama: 'BBBBBB',
            alamat: 'BBBBBB',
            jenisKelamin: 'BBBBBB',
            tempatLahir: 'BBBBBB',
            tanggalLahir: currentDate.format(DATE_TIME_FORMAT),
            waliMurid: 'BBBBBB',
            noTelephone: 'BBBBBB',
            tahunAjaran: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            tanggalLahir: currentDate
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Siswa', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
