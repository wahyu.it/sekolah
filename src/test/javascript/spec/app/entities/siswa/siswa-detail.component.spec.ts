import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { SiswaDetailComponent } from 'app/entities/siswa/siswa-detail.component';
import { Siswa } from 'app/shared/model/siswa.model';

describe('Component Tests', () => {
  describe('Siswa Management Detail Component', () => {
    let comp: SiswaDetailComponent;
    let fixture: ComponentFixture<SiswaDetailComponent>;
    const route = ({ data: of({ siswa: new Siswa(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [SiswaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SiswaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SiswaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load siswa on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.siswa).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
