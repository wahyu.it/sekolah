import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { SiswaUpdateComponent } from 'app/entities/siswa/siswa-update.component';
import { SiswaService } from 'app/entities/siswa/siswa.service';
import { Siswa } from 'app/shared/model/siswa.model';

describe('Component Tests', () => {
  describe('Siswa Management Update Component', () => {
    let comp: SiswaUpdateComponent;
    let fixture: ComponentFixture<SiswaUpdateComponent>;
    let service: SiswaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [SiswaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SiswaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SiswaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SiswaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Siswa(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Siswa();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
