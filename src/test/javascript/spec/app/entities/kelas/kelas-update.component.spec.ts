import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { KelasUpdateComponent } from 'app/entities/kelas/kelas-update.component';
import { KelasService } from 'app/entities/kelas/kelas.service';
import { Kelas } from 'app/shared/model/kelas.model';

describe('Component Tests', () => {
  describe('Kelas Management Update Component', () => {
    let comp: KelasUpdateComponent;
    let fixture: ComponentFixture<KelasUpdateComponent>;
    let service: KelasService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [KelasUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(KelasUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(KelasUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(KelasService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Kelas(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Kelas();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
