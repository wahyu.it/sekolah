import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PondokTestModule } from '../../../test.module';
import { KelasDetailComponent } from 'app/entities/kelas/kelas-detail.component';
import { Kelas } from 'app/shared/model/kelas.model';

describe('Component Tests', () => {
  describe('Kelas Management Detail Component', () => {
    let comp: KelasDetailComponent;
    let fixture: ComponentFixture<KelasDetailComponent>;
    const route = ({ data: of({ kelas: new Kelas(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PondokTestModule],
        declarations: [KelasDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(KelasDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(KelasDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load kelas on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.kelas).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
