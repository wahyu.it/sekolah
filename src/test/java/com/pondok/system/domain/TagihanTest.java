package com.pondok.system.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.pondok.system.web.rest.TestUtil;

public class TagihanTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tagihan.class);
        Tagihan tagihan1 = new Tagihan();
        tagihan1.setId(1L);
        Tagihan tagihan2 = new Tagihan();
        tagihan2.setId(tagihan1.getId());
        assertThat(tagihan1).isEqualTo(tagihan2);
        tagihan2.setId(2L);
        assertThat(tagihan1).isNotEqualTo(tagihan2);
        tagihan1.setId(null);
        assertThat(tagihan1).isNotEqualTo(tagihan2);
    }
}
