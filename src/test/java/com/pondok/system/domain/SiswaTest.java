package com.pondok.system.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.pondok.system.web.rest.TestUtil;

public class SiswaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Siswa.class);
        Siswa siswa1 = new Siswa();
        siswa1.setId(1L);
        Siswa siswa2 = new Siswa();
        siswa2.setId(siswa1.getId());
        assertThat(siswa1).isEqualTo(siswa2);
        siswa2.setId(2L);
        assertThat(siswa1).isNotEqualTo(siswa2);
        siswa1.setId(null);
        assertThat(siswa1).isNotEqualTo(siswa2);
    }
}
