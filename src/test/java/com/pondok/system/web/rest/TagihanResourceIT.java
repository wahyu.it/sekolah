package com.pondok.system.web.rest;

import com.pondok.system.PondokApp;
import com.pondok.system.domain.Tagihan;
import com.pondok.system.repository.TagihanRepository;
import com.pondok.system.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.pondok.system.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pondok.system.domain.enumeration.StatusBayar;
/**
 * Integration tests for the {@link TagihanResource} REST controller.
 */
@SpringBootTest(classes = PondokApp.class)
public class TagihanResourceIT {

    private static final String DEFAULT_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR = "BBBBBBBBBB";

    private static final String DEFAULT_TANGGAL = "BBBBBBBBBB";
    private static final String UPDATED_TANGGAL = "BBBBBBBBBB";

    private static final Long DEFAULT_TOTAL = 1L;
    private static final Long UPDATED_TOTAL = 2L;

    private static final Instant DEFAULT_JATUH_TEMPO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_JATUH_TEMPO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_TANGGAL_BAYAR = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TANGGAL_BAYAR = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final StatusBayar DEFAULT_STATUS = StatusBayar.LUNAS;
    private static final StatusBayar UPDATED_STATUS = StatusBayar.BELUMBAYAR;

    @Autowired
    private TagihanRepository tagihanRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTagihanMockMvc;

    private Tagihan tagihan;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
//        final TagihanResource tagihanResource = new TagihanResource(tagihanRepository);
//        this.restTagihanMockMvc = MockMvcBuilders.standaloneSetup(tagihanResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tagihan createEntity(EntityManager em) {
        Tagihan tagihan = new Tagihan()
            .nomor(DEFAULT_NOMOR)
            .tanggal(DEFAULT_TANGGAL)
            .jatuhTempo(DEFAULT_JATUH_TEMPO)
            .tanggalBayar(DEFAULT_TANGGAL_BAYAR)
            .status(DEFAULT_STATUS);
        return tagihan;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tagihan createUpdatedEntity(EntityManager em) {
        Tagihan tagihan = new Tagihan()
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .jatuhTempo(UPDATED_JATUH_TEMPO)
            .tanggalBayar(UPDATED_TANGGAL_BAYAR)
            .status(UPDATED_STATUS);
        return tagihan;
    }

    @BeforeEach
    public void initTest() {
        tagihan = createEntity(em);
    }

    @Test
    @Transactional
    public void createTagihan() throws Exception {
        int databaseSizeBeforeCreate = tagihanRepository.findAll().size();

        // Create the Tagihan
        restTagihanMockMvc.perform(post("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagihan)))
            .andExpect(status().isCreated());

        // Validate the Tagihan in the database
        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeCreate + 1);
        Tagihan testTagihan = tagihanList.get(tagihanList.size() - 1);
        assertThat(testTagihan.getNomor()).isEqualTo(DEFAULT_NOMOR);
        assertThat(testTagihan.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testTagihan.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testTagihan.getJatuhTempo()).isEqualTo(DEFAULT_JATUH_TEMPO);
        assertThat(testTagihan.getTanggalBayar()).isEqualTo(DEFAULT_TANGGAL_BAYAR);
        assertThat(testTagihan.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createTagihanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tagihanRepository.findAll().size();

        // Create the Tagihan with an existing ID
        tagihan.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTagihanMockMvc.perform(post("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagihan)))
            .andExpect(status().isBadRequest());

        // Validate the Tagihan in the database
        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNomorIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagihanRepository.findAll().size();
        // set the field null
        tagihan.setNomor(null);

        // Create the Tagihan, which fails.

        restTagihanMockMvc.perform(post("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagihan)))
            .andExpect(status().isBadRequest());

        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTanggalIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagihanRepository.findAll().size();
        // set the field null
        tagihan.setTanggal(null);

        // Create the Tagihan, which fails.

        restTagihanMockMvc.perform(post("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagihan)))
            .andExpect(status().isBadRequest());

        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTotalIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagihanRepository.findAll().size();
        // set the field null
        tagihan.setTotal(null);

        // Create the Tagihan, which fails.

        restTagihanMockMvc.perform(post("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagihan)))
            .andExpect(status().isBadRequest());

        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkJatuhTempoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagihanRepository.findAll().size();
        // set the field null
        tagihan.setJatuhTempo(null);

        // Create the Tagihan, which fails.

        restTagihanMockMvc.perform(post("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagihan)))
            .andExpect(status().isBadRequest());

        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTagihans() throws Exception {
        // Initialize the database
        tagihanRepository.saveAndFlush(tagihan);

        // Get all the tagihanList
        restTagihanMockMvc.perform(get("/api/tagihans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tagihan.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomor").value(hasItem(DEFAULT_NOMOR)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].jatuhTempo").value(hasItem(DEFAULT_JATUH_TEMPO.toString())))
            .andExpect(jsonPath("$.[*].tanggalBayar").value(hasItem(DEFAULT_TANGGAL_BAYAR.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @Test
    @Transactional
    public void getTagihan() throws Exception {
        // Initialize the database
        tagihanRepository.saveAndFlush(tagihan);

        // Get the tagihan
        restTagihanMockMvc.perform(get("/api/tagihans/{id}", tagihan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tagihan.getId().intValue()))
            .andExpect(jsonPath("$.nomor").value(DEFAULT_NOMOR))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.intValue()))
            .andExpect(jsonPath("$.jatuhTempo").value(DEFAULT_JATUH_TEMPO.toString()))
            .andExpect(jsonPath("$.tanggalBayar").value(DEFAULT_TANGGAL_BAYAR.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTagihan() throws Exception {
        // Get the tagihan
        restTagihanMockMvc.perform(get("/api/tagihans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTagihan() throws Exception {
        // Initialize the database
        tagihanRepository.saveAndFlush(tagihan);

        int databaseSizeBeforeUpdate = tagihanRepository.findAll().size();

        // Update the tagihan
        Tagihan updatedTagihan = tagihanRepository.findById(tagihan.getId()).get();
        // Disconnect from session so that the updates on updatedTagihan are not directly saved in db
        em.detach(updatedTagihan);
        updatedTagihan
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .jatuhTempo(UPDATED_JATUH_TEMPO)
            .tanggalBayar(UPDATED_TANGGAL_BAYAR)
            .status(UPDATED_STATUS);

        restTagihanMockMvc.perform(put("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTagihan)))
            .andExpect(status().isOk());

        // Validate the Tagihan in the database
        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeUpdate);
        Tagihan testTagihan = tagihanList.get(tagihanList.size() - 1);
        assertThat(testTagihan.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testTagihan.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testTagihan.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testTagihan.getJatuhTempo()).isEqualTo(UPDATED_JATUH_TEMPO);
        assertThat(testTagihan.getTanggalBayar()).isEqualTo(UPDATED_TANGGAL_BAYAR);
        assertThat(testTagihan.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingTagihan() throws Exception {
        int databaseSizeBeforeUpdate = tagihanRepository.findAll().size();

        // Create the Tagihan

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTagihanMockMvc.perform(put("/api/tagihans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagihan)))
            .andExpect(status().isBadRequest());

        // Validate the Tagihan in the database
        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTagihan() throws Exception {
        // Initialize the database
        tagihanRepository.saveAndFlush(tagihan);

        int databaseSizeBeforeDelete = tagihanRepository.findAll().size();

        // Delete the tagihan
        restTagihanMockMvc.perform(delete("/api/tagihans/{id}", tagihan.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Tagihan> tagihanList = tagihanRepository.findAll();
        assertThat(tagihanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
