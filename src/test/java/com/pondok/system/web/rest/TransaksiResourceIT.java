package com.pondok.system.web.rest;

import com.pondok.system.PondokApp;
import com.pondok.system.domain.Transaksi;
import com.pondok.system.repository.TransaksiRepository;
import com.pondok.system.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.pondok.system.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pondok.system.domain.enumeration.MetodeTransaksi;
import com.pondok.system.domain.enumeration.PaymentChanel;
import com.pondok.system.domain.enumeration.StatusTransaksi;
/**
 * Integration tests for the {@link TransaksiResource} REST controller.
 */
@SpringBootTest(classes = PondokApp.class)
public class TransaksiResourceIT {

    private static final String DEFAULT_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR = "BBBBBBBBBB";

    private static final Instant DEFAULT_TANGGAL = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TANGGAL = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final MetodeTransaksi DEFAULT_METODE = MetodeTransaksi.TUNAI;
    private static final MetodeTransaksi UPDATED_METODE = MetodeTransaksi.TRANSFER;

    private static final PaymentChanel DEFAULT_BANK = PaymentChanel.BCA;
    private static final PaymentChanel UPDATED_BANK = PaymentChanel.BNI;

    private static final String DEFAULT_NOMOR_REKENING = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR_REKENING = "BBBBBBBBBB";

    private static final String DEFAULT_VIRTUAL_ACCOUNT = "AAAAAAAAAA";
    private static final String UPDATED_VIRTUAL_ACCOUNT = "BBBBBBBBBB";

    private static final StatusTransaksi DEFAULT_STATUS = StatusTransaksi.SUKSES;
    private static final StatusTransaksi UPDATED_STATUS = StatusTransaksi.PROSES;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATE_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private TransaksiRepository transaksiRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTransaksiMockMvc;

    private Transaksi transaksi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
//        final TransaksiResource transaksiResource = new TransaksiResource(transaksiRepository);
//        this.restTransaksiMockMvc = MockMvcBuilders.standaloneSetup(transaksiResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transaksi createEntity(EntityManager em) {
        Transaksi transaksi = new Transaksi()
            .nomor(DEFAULT_NOMOR)
            .tanggal(DEFAULT_TANGGAL)
            .metode(DEFAULT_METODE)
            .nomorRekening(DEFAULT_NOMOR_REKENING)
            .virtualAccount(DEFAULT_VIRTUAL_ACCOUNT)
            .status(DEFAULT_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON);
        return transaksi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transaksi createUpdatedEntity(EntityManager em) {
        Transaksi transaksi = new Transaksi()
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .metode(UPDATED_METODE)
            .nomorRekening(UPDATED_NOMOR_REKENING)
            .virtualAccount(UPDATED_VIRTUAL_ACCOUNT)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);
        return transaksi;
    }

    @BeforeEach
    public void initTest() {
        transaksi = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransaksi() throws Exception {
        int databaseSizeBeforeCreate = transaksiRepository.findAll().size();

        // Create the Transaksi
        restTransaksiMockMvc.perform(post("/api/transaksis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaksi)))
            .andExpect(status().isCreated());

        // Validate the Transaksi in the database
        List<Transaksi> transaksiList = transaksiRepository.findAll();
        assertThat(transaksiList).hasSize(databaseSizeBeforeCreate + 1);
        Transaksi testTransaksi = transaksiList.get(transaksiList.size() - 1);
        assertThat(testTransaksi.getNomor()).isEqualTo(DEFAULT_NOMOR);
        assertThat(testTransaksi.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testTransaksi.getMetode()).isEqualTo(DEFAULT_METODE);
        assertThat(testTransaksi.getBank()).isEqualTo(DEFAULT_BANK);
        assertThat(testTransaksi.getNomorRekening()).isEqualTo(DEFAULT_NOMOR_REKENING);
        assertThat(testTransaksi.getVirtualAccount()).isEqualTo(DEFAULT_VIRTUAL_ACCOUNT);
        assertThat(testTransaksi.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTransaksi.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testTransaksi.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    public void createTransaksiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transaksiRepository.findAll().size();

        // Create the Transaksi with an existing ID
        transaksi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransaksiMockMvc.perform(post("/api/transaksis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaksi)))
            .andExpect(status().isBadRequest());

        // Validate the Transaksi in the database
        List<Transaksi> transaksiList = transaksiRepository.findAll();
        assertThat(transaksiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTransaksis() throws Exception {
        // Initialize the database
        transaksiRepository.saveAndFlush(transaksi);

        // Get all the transaksiList
        restTransaksiMockMvc.perform(get("/api/transaksis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transaksi.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomor").value(hasItem(DEFAULT_NOMOR)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].metode").value(hasItem(DEFAULT_METODE.toString())))
            .andExpect(jsonPath("$.[*].bank").value(hasItem(DEFAULT_BANK.toString())))
            .andExpect(jsonPath("$.[*].nomorRekening").value(hasItem(DEFAULT_NOMOR_REKENING)))
            .andExpect(jsonPath("$.[*].virtualAccount").value(hasItem(DEFAULT_VIRTUAL_ACCOUNT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(DEFAULT_UPDATE_ON.toString())));
    }
    
    @Test
    @Transactional
    public void getTransaksi() throws Exception {
        // Initialize the database
        transaksiRepository.saveAndFlush(transaksi);

        // Get the transaksi
        restTransaksiMockMvc.perform(get("/api/transaksis/{id}", transaksi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transaksi.getId().intValue()))
            .andExpect(jsonPath("$.nomor").value(DEFAULT_NOMOR))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.metode").value(DEFAULT_METODE.toString()))
            .andExpect(jsonPath("$.bank").value(DEFAULT_BANK.toString()))
            .andExpect(jsonPath("$.nomorRekening").value(DEFAULT_NOMOR_REKENING))
            .andExpect(jsonPath("$.virtualAccount").value(DEFAULT_VIRTUAL_ACCOUNT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(DEFAULT_UPDATE_ON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransaksi() throws Exception {
        // Get the transaksi
        restTransaksiMockMvc.perform(get("/api/transaksis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransaksi() throws Exception {
        // Initialize the database
        transaksiRepository.saveAndFlush(transaksi);

        int databaseSizeBeforeUpdate = transaksiRepository.findAll().size();

        // Update the transaksi
        Transaksi updatedTransaksi = transaksiRepository.findById(transaksi.getId()).get();
        // Disconnect from session so that the updates on updatedTransaksi are not directly saved in db
        em.detach(updatedTransaksi);
        updatedTransaksi
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .metode(UPDATED_METODE)
            .nomorRekening(UPDATED_NOMOR_REKENING)
            .virtualAccount(UPDATED_VIRTUAL_ACCOUNT)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restTransaksiMockMvc.perform(put("/api/transaksis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTransaksi)))
            .andExpect(status().isOk());

        // Validate the Transaksi in the database
        List<Transaksi> transaksiList = transaksiRepository.findAll();
        assertThat(transaksiList).hasSize(databaseSizeBeforeUpdate);
        Transaksi testTransaksi = transaksiList.get(transaksiList.size() - 1);
        assertThat(testTransaksi.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testTransaksi.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testTransaksi.getMetode()).isEqualTo(UPDATED_METODE);
        assertThat(testTransaksi.getBank()).isEqualTo(UPDATED_BANK);
        assertThat(testTransaksi.getNomorRekening()).isEqualTo(UPDATED_NOMOR_REKENING);
        assertThat(testTransaksi.getVirtualAccount()).isEqualTo(UPDATED_VIRTUAL_ACCOUNT);
        assertThat(testTransaksi.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTransaksi.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testTransaksi.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    public void updateNonExistingTransaksi() throws Exception {
        int databaseSizeBeforeUpdate = transaksiRepository.findAll().size();

        // Create the Transaksi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransaksiMockMvc.perform(put("/api/transaksis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaksi)))
            .andExpect(status().isBadRequest());

        // Validate the Transaksi in the database
        List<Transaksi> transaksiList = transaksiRepository.findAll();
        assertThat(transaksiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTransaksi() throws Exception {
        // Initialize the database
        transaksiRepository.saveAndFlush(transaksi);

        int databaseSizeBeforeDelete = transaksiRepository.findAll().size();

        // Delete the transaksi
        restTransaksiMockMvc.perform(delete("/api/transaksis/{id}", transaksi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Transaksi> transaksiList = transaksiRepository.findAll();
        assertThat(transaksiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
