package com.pondok.system.web.rest;

import com.pondok.system.PondokApp;
import com.pondok.system.domain.Kelas;
import com.pondok.system.repository.KelasRepository;
import com.pondok.system.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.pondok.system.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pondok.system.domain.enumeration.Jenjang;
/**
 * Integration tests for the {@link KelasResource} REST controller.
 */
@SpringBootTest(classes = PondokApp.class)
public class KelasResourceIT {

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final Jenjang DEFAULT_JENJANG = Jenjang.MA;
    private static final Jenjang UPDATED_JENJANG = Jenjang.MTS;

    @Autowired
    private KelasRepository kelasRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restKelasMockMvc;

    private Kelas kelas;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KelasResource kelasResource = new KelasResource(kelasRepository);
        this.restKelasMockMvc = MockMvcBuilders.standaloneSetup(kelasResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kelas createEntity(EntityManager em) {
        Kelas kelas = new Kelas()
            .nama(DEFAULT_NAMA)
            .jenjang(DEFAULT_JENJANG);
        return kelas;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kelas createUpdatedEntity(EntityManager em) {
        Kelas kelas = new Kelas()
            .nama(UPDATED_NAMA)
            .jenjang(UPDATED_JENJANG);
        return kelas;
    }

    @BeforeEach
    public void initTest() {
        kelas = createEntity(em);
    }

    @Test
    @Transactional
    public void createKelas() throws Exception {
        int databaseSizeBeforeCreate = kelasRepository.findAll().size();

        // Create the Kelas
        restKelasMockMvc.perform(post("/api/kelas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kelas)))
            .andExpect(status().isCreated());

        // Validate the Kelas in the database
        List<Kelas> kelasList = kelasRepository.findAll();
        assertThat(kelasList).hasSize(databaseSizeBeforeCreate + 1);
        Kelas testKelas = kelasList.get(kelasList.size() - 1);
        assertThat(testKelas.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testKelas.getJenjang()).isEqualTo(DEFAULT_JENJANG);
    }

    @Test
    @Transactional
    public void createKelasWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kelasRepository.findAll().size();

        // Create the Kelas with an existing ID
        kelas.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKelasMockMvc.perform(post("/api/kelas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kelas)))
            .andExpect(status().isBadRequest());

        // Validate the Kelas in the database
        List<Kelas> kelasList = kelasRepository.findAll();
        assertThat(kelasList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNamaIsRequired() throws Exception {
        int databaseSizeBeforeTest = kelasRepository.findAll().size();
        // set the field null
        kelas.setNama(null);

        // Create the Kelas, which fails.

        restKelasMockMvc.perform(post("/api/kelas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kelas)))
            .andExpect(status().isBadRequest());

        List<Kelas> kelasList = kelasRepository.findAll();
        assertThat(kelasList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllKelas() throws Exception {
        // Initialize the database
        kelasRepository.saveAndFlush(kelas);

        // Get all the kelasList
        restKelasMockMvc.perform(get("/api/kelas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kelas.getId().intValue())))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].jenjang").value(hasItem(DEFAULT_JENJANG.toString())));
    }
    
    @Test
    @Transactional
    public void getKelas() throws Exception {
        // Initialize the database
        kelasRepository.saveAndFlush(kelas);

        // Get the kelas
        restKelasMockMvc.perform(get("/api/kelas/{id}", kelas.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(kelas.getId().intValue()))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.jenjang").value(DEFAULT_JENJANG.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingKelas() throws Exception {
        // Get the kelas
        restKelasMockMvc.perform(get("/api/kelas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKelas() throws Exception {
        // Initialize the database
        kelasRepository.saveAndFlush(kelas);

        int databaseSizeBeforeUpdate = kelasRepository.findAll().size();

        // Update the kelas
        Kelas updatedKelas = kelasRepository.findById(kelas.getId()).get();
        // Disconnect from session so that the updates on updatedKelas are not directly saved in db
        em.detach(updatedKelas);
        updatedKelas
            .nama(UPDATED_NAMA)
            .jenjang(UPDATED_JENJANG);

        restKelasMockMvc.perform(put("/api/kelas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedKelas)))
            .andExpect(status().isOk());

        // Validate the Kelas in the database
        List<Kelas> kelasList = kelasRepository.findAll();
        assertThat(kelasList).hasSize(databaseSizeBeforeUpdate);
        Kelas testKelas = kelasList.get(kelasList.size() - 1);
        assertThat(testKelas.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testKelas.getJenjang()).isEqualTo(UPDATED_JENJANG);
    }

    @Test
    @Transactional
    public void updateNonExistingKelas() throws Exception {
        int databaseSizeBeforeUpdate = kelasRepository.findAll().size();

        // Create the Kelas

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKelasMockMvc.perform(put("/api/kelas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kelas)))
            .andExpect(status().isBadRequest());

        // Validate the Kelas in the database
        List<Kelas> kelasList = kelasRepository.findAll();
        assertThat(kelasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKelas() throws Exception {
        // Initialize the database
        kelasRepository.saveAndFlush(kelas);

        int databaseSizeBeforeDelete = kelasRepository.findAll().size();

        // Delete the kelas
        restKelasMockMvc.perform(delete("/api/kelas/{id}", kelas.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kelas> kelasList = kelasRepository.findAll();
        assertThat(kelasList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
