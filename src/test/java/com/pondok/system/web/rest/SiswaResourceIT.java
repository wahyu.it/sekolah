package com.pondok.system.web.rest;

import com.pondok.system.PondokApp;
import com.pondok.system.domain.Siswa;
import com.pondok.system.repository.SiswaRepository;
import com.pondok.system.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.pondok.system.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pondok.system.domain.enumeration.JenisKelamin;
/**
 * Integration tests for the {@link SiswaResource} REST controller.
 */
@SpringBootTest(classes = PondokApp.class)
public class SiswaResourceIT {

    private static final String DEFAULT_NIS = "AAAAAAAAAA";
    private static final String UPDATED_NIS = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_ALAMAT = "AAAAAAAAAA";
    private static final String UPDATED_ALAMAT = "BBBBBBBBBB";

    private static final JenisKelamin DEFAULT_JENIS_KELAMIN = JenisKelamin.LAKILAKI;
    private static final JenisKelamin UPDATED_JENIS_KELAMIN = JenisKelamin.PEREMPUAN;

    private static final String DEFAULT_TEMPAT_LAHIR = "AAAAAAAAAA";
    private static final String UPDATED_TEMPAT_LAHIR = "BBBBBBBBBB";

    private static final Instant DEFAULT_TANGGAL_LAHIR = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TANGGAL_LAHIR = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_WALI_MURID = "AAAAAAAAAA";
    private static final String UPDATED_WALI_MURID = "BBBBBBBBBB";

    private static final String DEFAULT_NO_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_NO_TELEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_TAHUN_AJARAN = "AAAAAAAAAA";
    private static final String UPDATED_TAHUN_AJARAN = "BBBBBBBBBB";

    @Autowired
    private SiswaRepository siswaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSiswaMockMvc;

    private Siswa siswa;

//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final SiswaResource siswaResource = new SiswaResource(siswaRepository);
//        this.restSiswaMockMvc = MockMvcBuilders.standaloneSetup(siswaResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Siswa createEntity(EntityManager em) {
        Siswa siswa = new Siswa()
            .nis(DEFAULT_NIS)
            .nama(DEFAULT_NAMA)
            .alamat(DEFAULT_ALAMAT)
            .jenisKelamin(DEFAULT_JENIS_KELAMIN)
            .tempatLahir(DEFAULT_TEMPAT_LAHIR)
            .tanggalLahir(DEFAULT_TANGGAL_LAHIR)
            .waliMurid(DEFAULT_WALI_MURID)
            .noTelephone(DEFAULT_NO_TELEPHONE)
            .tahunAjaran(DEFAULT_TAHUN_AJARAN);
        return siswa;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Siswa createUpdatedEntity(EntityManager em) {
        Siswa siswa = new Siswa()
            .nis(UPDATED_NIS)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .tempatLahir(UPDATED_TEMPAT_LAHIR)
            .tanggalLahir(UPDATED_TANGGAL_LAHIR)
            .waliMurid(UPDATED_WALI_MURID)
            .noTelephone(UPDATED_NO_TELEPHONE)
            .tahunAjaran(UPDATED_TAHUN_AJARAN);
        return siswa;
    }

    @BeforeEach
    public void initTest() {
        siswa = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiswa() throws Exception {
        int databaseSizeBeforeCreate = siswaRepository.findAll().size();

        // Create the Siswa
        restSiswaMockMvc.perform(post("/api/siswas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siswa)))
            .andExpect(status().isCreated());

        // Validate the Siswa in the database
        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeCreate + 1);
        Siswa testSiswa = siswaList.get(siswaList.size() - 1);
        assertThat(testSiswa.getNis()).isEqualTo(DEFAULT_NIS);
        assertThat(testSiswa.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testSiswa.getAlamat()).isEqualTo(DEFAULT_ALAMAT);
        assertThat(testSiswa.getJenisKelamin()).isEqualTo(DEFAULT_JENIS_KELAMIN);
        assertThat(testSiswa.getTempatLahir()).isEqualTo(DEFAULT_TEMPAT_LAHIR);
        assertThat(testSiswa.getTanggalLahir()).isEqualTo(DEFAULT_TANGGAL_LAHIR);
        assertThat(testSiswa.getWaliMurid()).isEqualTo(DEFAULT_WALI_MURID);
        assertThat(testSiswa.getNoTelephone()).isEqualTo(DEFAULT_NO_TELEPHONE);
        assertThat(testSiswa.getTahunAjaran()).isEqualTo(DEFAULT_TAHUN_AJARAN);
    }

    @Test
    @Transactional
    public void createSiswaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siswaRepository.findAll().size();

        // Create the Siswa with an existing ID
        siswa.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiswaMockMvc.perform(post("/api/siswas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siswa)))
            .andExpect(status().isBadRequest());

        // Validate the Siswa in the database
        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNisIsRequired() throws Exception {
        int databaseSizeBeforeTest = siswaRepository.findAll().size();
        // set the field null
        siswa.setNis(null);

        // Create the Siswa, which fails.

        restSiswaMockMvc.perform(post("/api/siswas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siswa)))
            .andExpect(status().isBadRequest());

        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamaIsRequired() throws Exception {
        int databaseSizeBeforeTest = siswaRepository.findAll().size();
        // set the field null
        siswa.setNama(null);

        // Create the Siswa, which fails.

        restSiswaMockMvc.perform(post("/api/siswas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siswa)))
            .andExpect(status().isBadRequest());

        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAlamatIsRequired() throws Exception {
        int databaseSizeBeforeTest = siswaRepository.findAll().size();
        // set the field null
        siswa.setAlamat(null);

        // Create the Siswa, which fails.

        restSiswaMockMvc.perform(post("/api/siswas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siswa)))
            .andExpect(status().isBadRequest());

        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiswas() throws Exception {
        // Initialize the database
        siswaRepository.saveAndFlush(siswa);

        // Get all the siswaList
        restSiswaMockMvc.perform(get("/api/siswas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siswa.getId().intValue())))
            .andExpect(jsonPath("$.[*].nis").value(hasItem(DEFAULT_NIS)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].alamat").value(hasItem(DEFAULT_ALAMAT)))
            .andExpect(jsonPath("$.[*].jenisKelamin").value(hasItem(DEFAULT_JENIS_KELAMIN.toString())))
            .andExpect(jsonPath("$.[*].tempatLahir").value(hasItem(DEFAULT_TEMPAT_LAHIR)))
            .andExpect(jsonPath("$.[*].tanggalLahir").value(hasItem(DEFAULT_TANGGAL_LAHIR.toString())))
            .andExpect(jsonPath("$.[*].waliMurid").value(hasItem(DEFAULT_WALI_MURID)))
            .andExpect(jsonPath("$.[*].noTelephone").value(hasItem(DEFAULT_NO_TELEPHONE)))
            .andExpect(jsonPath("$.[*].tahunAjaran").value(hasItem(DEFAULT_TAHUN_AJARAN)));
    }
    
    @Test
    @Transactional
    public void getSiswa() throws Exception {
        // Initialize the database
        siswaRepository.saveAndFlush(siswa);

        // Get the siswa
        restSiswaMockMvc.perform(get("/api/siswas/{id}", siswa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siswa.getId().intValue()))
            .andExpect(jsonPath("$.nis").value(DEFAULT_NIS))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.alamat").value(DEFAULT_ALAMAT))
            .andExpect(jsonPath("$.jenisKelamin").value(DEFAULT_JENIS_KELAMIN.toString()))
            .andExpect(jsonPath("$.tempatLahir").value(DEFAULT_TEMPAT_LAHIR))
            .andExpect(jsonPath("$.tanggalLahir").value(DEFAULT_TANGGAL_LAHIR.toString()))
            .andExpect(jsonPath("$.waliMurid").value(DEFAULT_WALI_MURID))
            .andExpect(jsonPath("$.noTelephone").value(DEFAULT_NO_TELEPHONE))
            .andExpect(jsonPath("$.tahunAjaran").value(DEFAULT_TAHUN_AJARAN));
    }

    @Test
    @Transactional
    public void getNonExistingSiswa() throws Exception {
        // Get the siswa
        restSiswaMockMvc.perform(get("/api/siswas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiswa() throws Exception {
        // Initialize the database
        siswaRepository.saveAndFlush(siswa);

        int databaseSizeBeforeUpdate = siswaRepository.findAll().size();

        // Update the siswa
        Siswa updatedSiswa = siswaRepository.findById(siswa.getId()).get();
        // Disconnect from session so that the updates on updatedSiswa are not directly saved in db
        em.detach(updatedSiswa);
        updatedSiswa
            .nis(UPDATED_NIS)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .tempatLahir(UPDATED_TEMPAT_LAHIR)
            .tanggalLahir(UPDATED_TANGGAL_LAHIR)
            .waliMurid(UPDATED_WALI_MURID)
            .noTelephone(UPDATED_NO_TELEPHONE)
            .tahunAjaran(UPDATED_TAHUN_AJARAN);

        restSiswaMockMvc.perform(put("/api/siswas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiswa)))
            .andExpect(status().isOk());

        // Validate the Siswa in the database
        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeUpdate);
        Siswa testSiswa = siswaList.get(siswaList.size() - 1);
        assertThat(testSiswa.getNis()).isEqualTo(UPDATED_NIS);
        assertThat(testSiswa.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testSiswa.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testSiswa.getJenisKelamin()).isEqualTo(UPDATED_JENIS_KELAMIN);
        assertThat(testSiswa.getTempatLahir()).isEqualTo(UPDATED_TEMPAT_LAHIR);
        assertThat(testSiswa.getTanggalLahir()).isEqualTo(UPDATED_TANGGAL_LAHIR);
        assertThat(testSiswa.getWaliMurid()).isEqualTo(UPDATED_WALI_MURID);
        assertThat(testSiswa.getNoTelephone()).isEqualTo(UPDATED_NO_TELEPHONE);
        assertThat(testSiswa.getTahunAjaran()).isEqualTo(UPDATED_TAHUN_AJARAN);
    }

    @Test
    @Transactional
    public void updateNonExistingSiswa() throws Exception {
        int databaseSizeBeforeUpdate = siswaRepository.findAll().size();

        // Create the Siswa

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSiswaMockMvc.perform(put("/api/siswas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siswa)))
            .andExpect(status().isBadRequest());

        // Validate the Siswa in the database
        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSiswa() throws Exception {
        // Initialize the database
        siswaRepository.saveAndFlush(siswa);

        int databaseSizeBeforeDelete = siswaRepository.findAll().size();

        // Delete the siswa
        restSiswaMockMvc.perform(delete("/api/siswas/{id}", siswa.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Siswa> siswaList = siswaRepository.findAll();
        assertThat(siswaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
