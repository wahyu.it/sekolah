package com.pondok.system.web.rest;

import com.pondok.system.PondokApp;
import com.pondok.system.domain.BesarBayar;
import com.pondok.system.repository.BesarBayarRepository;
import com.pondok.system.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.pondok.system.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BesarBayarResource} REST controller.
 */
@SpringBootTest(classes = PondokApp.class)
public class BesarBayarResourceIT {

    private static final Long DEFAULT_NOMINAL = 1L;
    private static final Long UPDATED_NOMINAL = 2L;

    @Autowired
    private BesarBayarRepository besarBayarRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBesarBayarMockMvc;

    private BesarBayar besarBayar;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BesarBayarResource besarBayarResource = new BesarBayarResource(besarBayarRepository);
        this.restBesarBayarMockMvc = MockMvcBuilders.standaloneSetup(besarBayarResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BesarBayar createEntity(EntityManager em) {
        BesarBayar besarBayar = new BesarBayar()
            .nominal(DEFAULT_NOMINAL);
        return besarBayar;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BesarBayar createUpdatedEntity(EntityManager em) {
        BesarBayar besarBayar = new BesarBayar()
            .nominal(UPDATED_NOMINAL);
        return besarBayar;
    }

    @BeforeEach
    public void initTest() {
        besarBayar = createEntity(em);
    }

    @Test
    @Transactional
    public void createBesarBayar() throws Exception {
        int databaseSizeBeforeCreate = besarBayarRepository.findAll().size();

        // Create the BesarBayar
        restBesarBayarMockMvc.perform(post("/api/besar-bayars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(besarBayar)))
            .andExpect(status().isCreated());

        // Validate the BesarBayar in the database
        List<BesarBayar> besarBayarList = besarBayarRepository.findAll();
        assertThat(besarBayarList).hasSize(databaseSizeBeforeCreate + 1);
        BesarBayar testBesarBayar = besarBayarList.get(besarBayarList.size() - 1);
        assertThat(testBesarBayar.getNominal()).isEqualTo(DEFAULT_NOMINAL);
    }

    @Test
    @Transactional
    public void createBesarBayarWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = besarBayarRepository.findAll().size();

        // Create the BesarBayar with an existing ID
        besarBayar.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBesarBayarMockMvc.perform(post("/api/besar-bayars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(besarBayar)))
            .andExpect(status().isBadRequest());

        // Validate the BesarBayar in the database
        List<BesarBayar> besarBayarList = besarBayarRepository.findAll();
        assertThat(besarBayarList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBesarBayars() throws Exception {
        // Initialize the database
        besarBayarRepository.saveAndFlush(besarBayar);

        // Get all the besarBayarList
        restBesarBayarMockMvc.perform(get("/api/besar-bayars?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(besarBayar.getId().intValue())))
            .andExpect(jsonPath("$.[*].nominal").value(hasItem(DEFAULT_NOMINAL.intValue())));
    }
    
    @Test
    @Transactional
    public void getBesarBayar() throws Exception {
        // Initialize the database
        besarBayarRepository.saveAndFlush(besarBayar);

        // Get the besarBayar
        restBesarBayarMockMvc.perform(get("/api/besar-bayars/{id}", besarBayar.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(besarBayar.getId().intValue()))
            .andExpect(jsonPath("$.nominal").value(DEFAULT_NOMINAL.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBesarBayar() throws Exception {
        // Get the besarBayar
        restBesarBayarMockMvc.perform(get("/api/besar-bayars/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBesarBayar() throws Exception {
        // Initialize the database
        besarBayarRepository.saveAndFlush(besarBayar);

        int databaseSizeBeforeUpdate = besarBayarRepository.findAll().size();

        // Update the besarBayar
        BesarBayar updatedBesarBayar = besarBayarRepository.findById(besarBayar.getId()).get();
        // Disconnect from session so that the updates on updatedBesarBayar are not directly saved in db
        em.detach(updatedBesarBayar);
        updatedBesarBayar
            .nominal(UPDATED_NOMINAL);

        restBesarBayarMockMvc.perform(put("/api/besar-bayars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBesarBayar)))
            .andExpect(status().isOk());

        // Validate the BesarBayar in the database
        List<BesarBayar> besarBayarList = besarBayarRepository.findAll();
        assertThat(besarBayarList).hasSize(databaseSizeBeforeUpdate);
        BesarBayar testBesarBayar = besarBayarList.get(besarBayarList.size() - 1);
        assertThat(testBesarBayar.getNominal()).isEqualTo(UPDATED_NOMINAL);
    }

    @Test
    @Transactional
    public void updateNonExistingBesarBayar() throws Exception {
        int databaseSizeBeforeUpdate = besarBayarRepository.findAll().size();

        // Create the BesarBayar

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBesarBayarMockMvc.perform(put("/api/besar-bayars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(besarBayar)))
            .andExpect(status().isBadRequest());

        // Validate the BesarBayar in the database
        List<BesarBayar> besarBayarList = besarBayarRepository.findAll();
        assertThat(besarBayarList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBesarBayar() throws Exception {
        // Initialize the database
        besarBayarRepository.saveAndFlush(besarBayar);

        int databaseSizeBeforeDelete = besarBayarRepository.findAll().size();

        // Delete the besarBayar
        restBesarBayarMockMvc.perform(delete("/api/besar-bayars/{id}", besarBayar.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BesarBayar> besarBayarList = besarBayarRepository.findAll();
        assertThat(besarBayarList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
