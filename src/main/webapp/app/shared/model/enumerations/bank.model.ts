export const enum Bank {
  VA_CIMB,
  VA_PERMATA,
  VA_FINPAY,
  VA_BCA,
  VA_MUAMALAT
}
