import { Moment } from 'moment';
import { JenisKelamin } from 'app/shared/model/enumerations/jenis-kelamin.model';

export interface IProfil {
  userId?: number;
  nis?: string;
  nama?: string;
  alamat?: string;
  jenisKelamin?: JenisKelamin;
  tempatLahir?: string;
  tanggalLahir?: Moment;
  waliMurid?: string;
  noTelephone?: string;
  tahunAjaran?: string;
  kelas?: string;
  firstName?: string;
  lastName?: string;
  siswaId?: number;
  status?: string;
}

export class Profil implements IProfil {
  constructor(
    public userId?: number,
    public nis?: string,
    public nama?: string,
    public alamat?: string,
    public jenisKelamin?: JenisKelamin,
    public tempatLahir?: string,
    public tanggalLahir?: Moment,
    public waliMurid?: string,
    public noTelephone?: string,
    public tahunAjaran?: string,
    public kelas?: string,
    public firstName?: string,
    public lastName?: string,
    public siswaId?: number,
    public status?: string
  ) {}
}
