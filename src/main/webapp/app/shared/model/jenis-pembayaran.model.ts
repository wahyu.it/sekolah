export interface IJenisPembayaran {
  id?: number;
  jenis?: string;
  metode?: string;
}

export class JenisPembayaran implements IJenisPembayaran {
  constructor(public id?: number, public jenis?: string, public metode?: string) {}
}
