export interface IPaymentChanel {
  id?: number;
  name?: string;
}

export class PaymentChanel implements IPaymentChanel {
  constructor(public id?: number, public name?: string) {}
}
