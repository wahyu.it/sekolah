import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { PondokSharedModule } from 'app/shared/shared.module';
import { PondokCoreModule } from 'app/core/core.module';
import { PondokAppRoutingModule } from './app-routing.module';
import { PondokHomeModule } from './home/home.module';
import { PondokEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    PondokSharedModule,
    PondokCoreModule,
    PondokHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    PondokEntityModule,
    PondokAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class PondokAppModule {}
