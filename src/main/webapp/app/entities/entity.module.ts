import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'siswa',
        loadChildren: () => import('./siswa/siswa.module').then(m => m.PondokSiswaModule)
      },
      {
        path: 'kelas',
        loadChildren: () => import('./kelas/kelas.module').then(m => m.PondokKelasModule)
      },
      {
        path: 'jenis-pembayaran',
        loadChildren: () => import('./jenis-pembayaran/jenis-pembayaran.module').then(m => m.PondokJenisPembayaranModule)
      },
      {
        path: 'besar-bayar',
        loadChildren: () => import('./besar-bayar/besar-bayar.module').then(m => m.PondokBesarBayarModule)
      },
      {
        path: 'tagihan',
        loadChildren: () => import('./tagihan/tagihan.module').then(m => m.PondokTagihanModule)
      },
      {
        path: 'transaksi',
        loadChildren: () => import('./transaksi/transaksi.module').then(m => m.PondokTransaksiModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class PondokEntityModule {}
