import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISiswa } from 'app/shared/model/siswa.model';
import { IProfil } from 'app/shared/model/profil.model';

type EntityResponseType = HttpResponse<ISiswa>;
type EntityArrayResponseType = HttpResponse<ISiswa[]>;

@Injectable({ providedIn: 'root' })
export class SiswaService {
  public resourceUrl = SERVER_API_URL + 'api/siswas';

  constructor(protected http: HttpClient) {}

  create(siswa: ISiswa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(siswa);
    return this.http
      .post<ISiswa>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(siswa: ISiswa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(siswa);
    return this.http
      .put<ISiswa>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISiswa>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISiswa[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(siswa: ISiswa): ISiswa {
    const copy: ISiswa = Object.assign({}, siswa, {
      tanggalLahir: siswa.tanggalLahir && siswa.tanggalLahir.isValid() ? siswa.tanggalLahir.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.tanggalLahir = res.body.tanggalLahir ? moment(res.body.tanggalLahir) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((siswa: ISiswa) => {
        siswa.tanggalLahir = siswa.tanggalLahir ? moment(siswa.tanggalLahir) : undefined;
      });
    }
    return res;
  }

  authorities(): Observable<string[]> {
    return this.http.get<string[]>(SERVER_API_URL + 'api/users/authorities');
  }

  userProfil(): Observable<IProfil> {
    return this.http.get<IProfil>(SERVER_API_URL + 'api/siswas/profil');
  }

  beasiswa(id: any, tipe: string): Observable<EntityResponseType> {
    console.log('cek checkouton3' + tipe);
    return this.http
      .put<ISiswa>(`${this.resourceUrl}/beasiswa/${id}`, tipe, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  loundry(id: any, tipe: string): Observable<EntityResponseType> {
    console.log('cek checkouton3' + tipe);
    return this.http
      .put<ISiswa>(`${this.resourceUrl}/loundry/${id}`, tipe, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
}
