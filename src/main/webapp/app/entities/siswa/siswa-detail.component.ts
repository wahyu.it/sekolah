import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISiswa } from 'app/shared/model/siswa.model';

@Component({
  selector: 'jhi-siswa-detail',
  templateUrl: './siswa-detail.component.html'
})
export class SiswaDetailComponent implements OnInit {
  siswa: ISiswa | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ siswa }) => {
      this.siswa = siswa;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
