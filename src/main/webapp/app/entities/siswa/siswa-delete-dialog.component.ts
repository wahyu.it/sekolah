import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISiswa } from 'app/shared/model/siswa.model';
import { SiswaService } from './siswa.service';

@Component({
  templateUrl: './siswa-delete-dialog.component.html'
})
export class SiswaDeleteDialogComponent {
  siswa?: ISiswa;

  constructor(protected siswaService: SiswaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.siswaService.delete(id).subscribe(() => {
      this.eventManager.broadcast('siswaListModification');
      this.activeModal.close();
    });
  }
}
