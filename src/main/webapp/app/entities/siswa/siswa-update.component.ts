import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISiswa, Siswa } from 'app/shared/model/siswa.model';
import { SiswaService } from './siswa.service';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from 'app/entities/kelas/kelas.service';

@Component({
  selector: 'jhi-siswa-update',
  templateUrl: './siswa-update.component.html',
  styleUrls: ['siswa.scss']
})
export class SiswaUpdateComponent implements OnInit {
  isSaving = false;

  kelas: IKelas[] = [];

  editForm = this.fb.group({
    id: [],
    nis: [null, [Validators.required]],
    nama: [null, [Validators.required]],
    alamat: [],
    jenisKelamin: [],
    tempatLahir: [],
    tanggalLahir: [],
    waliMurid: [],
    noTelephone: [],
    tahunAjaran: [],
    beasiswa: [],
    loundry: [],
    jenisBeasiswa: [],
    jmlBulan: [],
    kelas: [null, [Validators.required]]
  });

  constructor(
    protected siswaService: SiswaService,
    protected kelasService: KelasService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ siswa }) => {
      this.updateForm(siswa);

      this.kelasService
        .query({ filter: 'siswa-is-null' })
        .pipe(
          map((res: HttpResponse<IKelas[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IKelas[]) => {
          if (!siswa.kelas || !siswa.kelas.id) {
            this.kelas = resBody;
          } else {
            this.kelasService
              .find(siswa.kelas.id)
              .pipe(
                map((subRes: HttpResponse<IKelas>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IKelas[]) => {
                this.kelas = concatRes;
              });
          }
        });
    });
  }

  beasiswa(id: number, type: boolean): void {
    if (type === true) {
      console.log('updte1' + id);
      this.subscribeToSaveResponse(this.siswaService.beasiswa(id, 'true'));
    }
    if (type === false) {
      console.log('updte2' + id);
      this.subscribeToSaveResponse(this.siswaService.beasiswa(id, 'false'));
    }
  }

  loundry(id: number, type: boolean): void {
    if (type === true) {
      console.log('updte1' + id);
      this.subscribeToSaveResponse(this.siswaService.loundry(id, 'true'));
    }
    if (type === false) {
      console.log('updte2' + id);
      this.subscribeToSaveResponse(this.siswaService.loundry(id, 'false'));
    }
  }

  updateForm(siswa: ISiswa): void {
    this.editForm.patchValue({
      id: siswa.id,
      nis: siswa.nis,
      nama: siswa.nama,
      alamat: siswa.alamat,
      jenisKelamin: siswa.jenisKelamin,
      tempatLahir: siswa.tempatLahir,
      tanggalLahir: siswa.tanggalLahir != null ? siswa.tanggalLahir.format(DATE_TIME_FORMAT) : null,
      waliMurid: siswa.waliMurid,
      noTelephone: siswa.noTelephone,
      tahunAjaran: siswa.tahunAjaran,
      beasiswa: siswa.beasiswa,
      loundry: siswa.loundry,
      jenisBeasiswa: siswa.jenisBeasiswa,
      jmlBulan: siswa.jmlBulan,
      kelas: siswa.kelas
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const siswa = this.createFromForm();
    if (siswa.id !== undefined) {
      this.subscribeToSaveResponse(this.siswaService.update(siswa));
    } else {
      this.subscribeToSaveResponse(this.siswaService.create(siswa));
    }
  }

  private createFromForm(): ISiswa {
    return {
      ...new Siswa(),
      id: this.editForm.get(['id'])!.value,
      nis: this.editForm.get(['nis'])!.value,
      nama: this.editForm.get(['nama'])!.value,
      alamat: this.editForm.get(['alamat'])!.value,
      jenisKelamin: this.editForm.get(['jenisKelamin'])!.value,
      tempatLahir: this.editForm.get(['tempatLahir'])!.value,
      tanggalLahir:
        this.editForm.get(['tanggalLahir'])!.value != null
          ? moment(this.editForm.get(['tanggalLahir'])!.value, DATE_TIME_FORMAT)
          : undefined,
      waliMurid: this.editForm.get(['waliMurid'])!.value,
      noTelephone: this.editForm.get(['noTelephone'])!.value,
      tahunAjaran: this.editForm.get(['tahunAjaran'])!.value,
      beasiswa: this.editForm.get(['beasiswa'])!.value,
      loundry: this.editForm.get(['loundry'])!.value,
      jenisBeasiswa: this.editForm.get(['jenisBeasiswa'])!.value,
      jmlBulan: this.editForm.get(['jmlBulan'])!.value,
      kelas: this.editForm.get(['kelas'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISiswa>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IKelas): any {
    return item.id;
  }
}
