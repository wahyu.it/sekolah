import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PondokSharedModule } from 'app/shared/shared.module';
import { SiswaComponent } from './siswa.component';
import { SiswaDetailComponent } from './siswa-detail.component';
import { SiswaUpdateComponent } from './siswa-update.component';
import { SiswaDeleteDialogComponent } from './siswa-delete-dialog.component';
import { siswaRoute } from './siswa.route';

@NgModule({
  imports: [PondokSharedModule, RouterModule.forChild(siswaRoute)],
  declarations: [SiswaComponent, SiswaDetailComponent, SiswaUpdateComponent, SiswaDeleteDialogComponent],
  entryComponents: [SiswaDeleteDialogComponent]
})
export class PondokSiswaModule {}
