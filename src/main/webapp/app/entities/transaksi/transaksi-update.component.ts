import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ITransaksi, Transaksi } from 'app/shared/model/transaksi.model';
import { TransaksiService } from './transaksi.service';
import { ITagihan } from 'app/shared/model/tagihan.model';
import { TagihanService } from 'app/entities/tagihan/tagihan.service';

@Component({
  selector: 'jhi-transaksi-update',
  templateUrl: './transaksi-update.component.html'
})
export class TransaksiUpdateComponent implements OnInit {
  isSaving = false;

  tagihans: ITagihan[] = [];

  editForm = this.fb.group({
    id: [],
    nomor: [],
    tanggal: [],
    metode: [],
    bank: [],
    nomorRekening: [],
    virtualAccount: [],
    status: [],
    updateBy: [],
    updateOn: [],
    tagihan: []
  });

  constructor(
    protected transaksiService: TransaksiService,
    protected tagihanService: TagihanService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ transaksi }) => {
      this.updateForm(transaksi);

      this.tagihanService
        .query()
        .pipe(
          map((res: HttpResponse<ITagihan[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: ITagihan[]) => (this.tagihans = resBody));
    });
  }

  updateForm(transaksi: ITransaksi): void {
    this.editForm.patchValue({
      id: transaksi.id,
      nomor: transaksi.nomor,
      tanggal: transaksi.tanggal != null ? transaksi.tanggal.format(DATE_TIME_FORMAT) : null,
      metode: transaksi.metode,
      bank: transaksi.bank,
      nomorRekening: transaksi.nomorRekening,
      virtualAccount: transaksi.virtualAccount,
      status: transaksi.status,
      updateBy: transaksi.updateBy,
      updateOn: transaksi.updateOn != null ? transaksi.updateOn.format(DATE_TIME_FORMAT) : null,
      tagihan: transaksi.tagihan
    });
  }

  previousState(): void {
    window.history.back();
  }

  /*  save(): void {
    this.isSaving = true;
    const transaksi = this.createFromForm();
    if (transaksi.id !== undefined) {
      this.subscribeToSaveResponse(this.transaksiService.update(transaksi));
    } else {
      this.subscribeToSaveResponse(this.transaksiService.create(transaksi));
    }
  }*/

  private createFromForm(): ITransaksi {
    return {
      ...new Transaksi(),
      id: this.editForm.get(['id'])!.value,
      nomor: this.editForm.get(['nomor'])!.value,
      tanggal: this.editForm.get(['tanggal'])!.value != null ? moment(this.editForm.get(['tanggal'])!.value, DATE_TIME_FORMAT) : undefined,
      metode: this.editForm.get(['metode'])!.value,
      bank: this.editForm.get(['bank'])!.value,
      nomorRekening: this.editForm.get(['nomorRekening'])!.value,
      virtualAccount: this.editForm.get(['virtualAccount'])!.value,
      status: this.editForm.get(['status'])!.value,
      updateBy: this.editForm.get(['updateBy'])!.value,
      updateOn:
        this.editForm.get(['updateOn'])!.value != null ? moment(this.editForm.get(['updateOn'])!.value, DATE_TIME_FORMAT) : undefined,
      tagihan: this.editForm.get(['tagihan'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransaksi>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITagihan): any {
    return item.id;
  }
}
