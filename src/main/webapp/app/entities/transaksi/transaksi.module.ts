import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PondokSharedModule } from 'app/shared/shared.module';
import { TransaksiComponent } from './transaksi.component';
import { TransaksiDetailComponent } from './transaksi-detail.component';
import { TransaksiUpdateComponent } from './transaksi-update.component';
import { TransaksiDeleteDialogComponent } from './transaksi-delete-dialog.component';
import { transaksiRoute } from './transaksi.route';

@NgModule({
  imports: [PondokSharedModule, RouterModule.forChild(transaksiRoute)],
  declarations: [TransaksiComponent, TransaksiDetailComponent, TransaksiUpdateComponent, TransaksiDeleteDialogComponent],
  entryComponents: [TransaksiDeleteDialogComponent]
})
export class PondokTransaksiModule {}
