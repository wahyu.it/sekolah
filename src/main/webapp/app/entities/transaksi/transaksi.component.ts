import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TransaksiService } from './transaksi.service';
import { FormBuilder } from '@angular/forms';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';

@Component({
  selector: 'jhi-transaksi',
  templateUrl: './transaksi.component.html'
})
export class TransaksiComponent implements OnInit, OnDestroy {
  isSaving = false;
  transaksis?: ITagihanDTO[] | null = null;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  skey!: string;
  svalue!: string;
  previousPage: number | undefined;

  editForm = this.fb.group({
    pNis: [],
    pSiswa: [],
    pOptions: 1
  });

  constructor(
    protected transaksiService: TransaksiService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  loadAll(): void {
    if (
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== null && this.svalue !== null)
    ) {
      console.log('data param');
      this.searchOption = 1;
      this.transaksiService
        .query({
          page: this.page - 1,
          skey: this.skey,
          svalue: this.svalue,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihanDTO[]>) => this.onSuccess(res.body, res.headers));
      console.log('data tagihan:');
      console.log('this.skey: ' + this.skey);
      console.log('this.svalue: ' + this.svalue);
      console.log('this.page: ' + this.page);
    } else {
      console.log('data default');
      this.transaksiService
        .query({
          page: this.page - 1,
          skey: null,
          svalue: null,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihanDTO[]>) => this.onSuccess(res.body, res.headers));
    }
  }

  loadPage(page?: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
      this.loadAll();
    }
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.previousPage = data.pagingParams.page;
      this.predicate = data.pagingParams.predicate;
      this.previousPage = data.pagingParams.page;
      this.loadAll();
    });
    this.registerChangeInTransaksis();
    this.skey = '';
    this.svalue = '';
  }

  previousState(): void {
    window.history.back();
  }

  clear(): void {
    this.editForm.patchValue({
      pNis: null,
      pSiswa: null
    });
  }

  searchByParam(paramName: string): void {
    this.skey = '';
    this.svalue = '';

    if (paramName === 'nis' && this.editForm.get(['pNis'])!.value !== null) {
      this.skey = 'nis';
      this.svalue = this.editForm.get(['pNis'])!.value;
    } else if (paramName === 'siswa' && this.editForm.get(['pSiswa'])!.value !== null) {
      this.skey = 'siswa';
      this.svalue = this.editForm.get(['pSiswa'])!.value;
    }

    console.log('searchByParameter by ' + this.skey + ': ' + this.svalue);

    if (
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== null && this.svalue !== null)
    ) {
      console.log('data param');
      this.searchOption = 1;
      this.transaksiService
        .query({
          page: this.page - 1,
          skey: this.skey,
          svalue: this.svalue,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihanDTO[]>) => this.onSuccess(res.body, res.headers));
      console.log('data tagihan:');
      console.log('this.skey: ' + this.skey);
      console.log('this.svalue: ' + this.svalue);
      console.log('this.page: ' + this.page);
    } else {
      console.log('data default');
      this.transaksiService
        .query({
          page: this.page - 1,
          skey: null,
          svalue: null,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihanDTO[]>) => this.onSuccess(res.body, res.headers));
    }
  }

  optionSearch(): void {
    this.searchOption = this.editForm.get(['pOptions'])!.value;
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITagihanDTO): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTransaksis(): void {
    this.eventSubscriber = this.eventManager.subscribe('transaksiListModification', () => this.loadAll());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private onSuccess(users: ITagihanDTO[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.transaksis = users;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
}
