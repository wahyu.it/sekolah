import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { TransaksiService } from './transaksi.service';
import { TransaksiComponent } from './transaksi.component';
import { TransaksiDetailComponent } from './transaksi-detail.component';
import { TransaksiUpdateComponent } from './transaksi-update.component';
import { ITagihanDTO, TagihanDTO } from 'app/shared/model/tagihanDTO.model';

@Injectable({ providedIn: 'root' })
export class TransaksiResolve implements Resolve<ITagihanDTO> {
  constructor(private service: TransaksiService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITagihanDTO> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((transaksi: HttpResponse<TagihanDTO>) => {
          if (transaksi.body) {
            return of(transaksi.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TagihanDTO());
  }
}

export const transaksiRoute: Routes = [
  {
    path: '',
    component: TransaksiComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Transaksis'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TransaksiDetailComponent,
    resolve: {
      transaksi: TransaksiResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Transaksis'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TransaksiUpdateComponent,
    resolve: {
      transaksi: TransaksiResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Transaksis'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TransaksiUpdateComponent,
    resolve: {
      transaksi: TransaksiResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Transaksis'
    },
    canActivate: [UserRouteAccessService]
  }
];
