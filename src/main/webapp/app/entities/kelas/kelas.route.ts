import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IKelas, Kelas } from 'app/shared/model/kelas.model';
import { KelasService } from './kelas.service';
import { KelasComponent } from './kelas.component';
import { KelasDetailComponent } from './kelas-detail.component';
import { KelasUpdateComponent } from './kelas-update.component';

@Injectable({ providedIn: 'root' })
export class KelasResolve implements Resolve<IKelas> {
  constructor(private service: KelasService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IKelas> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((kelas: HttpResponse<Kelas>) => {
          if (kelas.body) {
            return of(kelas.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Kelas());
  }
}

export const kelasRoute: Routes = [
  {
    path: '',
    component: KelasComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Kelas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: KelasDetailComponent,
    resolve: {
      kelas: KelasResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Kelas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: KelasUpdateComponent,
    resolve: {
      kelas: KelasResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Kelas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: KelasUpdateComponent,
    resolve: {
      kelas: KelasResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Kelas'
    },
    canActivate: [UserRouteAccessService]
  }
];
