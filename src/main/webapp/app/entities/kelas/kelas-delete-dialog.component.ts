import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from './kelas.service';

@Component({
  templateUrl: './kelas-delete-dialog.component.html'
})
export class KelasDeleteDialogComponent {
  kelas?: IKelas;

  constructor(protected kelasService: KelasService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.kelasService.delete(id).subscribe(() => {
      this.eventManager.broadcast('kelasListModification');
      this.activeModal.close();
    });
  }
}
