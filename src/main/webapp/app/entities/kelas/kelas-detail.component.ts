import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKelas } from 'app/shared/model/kelas.model';

@Component({
  selector: 'jhi-kelas-detail',
  templateUrl: './kelas-detail.component.html'
})
export class KelasDetailComponent implements OnInit {
  kelas: IKelas | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ kelas }) => {
      this.kelas = kelas;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
