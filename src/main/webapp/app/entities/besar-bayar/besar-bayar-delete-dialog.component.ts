import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBesarBayar } from 'app/shared/model/besar-bayar.model';
import { BesarBayarService } from './besar-bayar.service';

@Component({
  templateUrl: './besar-bayar-delete-dialog.component.html'
})
export class BesarBayarDeleteDialogComponent {
  besarBayar?: IBesarBayar;

  constructor(
    protected besarBayarService: BesarBayarService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.besarBayarService.delete(id).subscribe(() => {
      this.eventManager.broadcast('besarBayarListModification');
      this.activeModal.close();
    });
  }
}
