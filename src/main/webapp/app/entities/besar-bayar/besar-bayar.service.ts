import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBesarBayar } from 'app/shared/model/besar-bayar.model';

type EntityResponseType = HttpResponse<IBesarBayar>;
type EntityArrayResponseType = HttpResponse<IBesarBayar[]>;

@Injectable({ providedIn: 'root' })
export class BesarBayarService {
  public resourceUrl = SERVER_API_URL + 'api/besar-bayars';
  public resourceUrlGenerate = SERVER_API_URL + 'api/tagihans/generatepartial';

  constructor(protected http: HttpClient) {}

  create(besarBayar: IBesarBayar): Observable<EntityResponseType> {
    return this.http.post<IBesarBayar>(this.resourceUrl, besarBayar, { observe: 'response' });
  }

  update(besarBayar: IBesarBayar): Observable<EntityResponseType> {
    return this.http.put<IBesarBayar>(this.resourceUrl, besarBayar, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBesarBayar>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBesarBayar[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  generate(id: number, req?: any): Observable<HttpResponse<{}>> {
    console.log('kelas id service:' + id);
    return this.http.put(`${this.resourceUrlGenerate}/${id}`, req, { observe: 'response' });
  }
}
