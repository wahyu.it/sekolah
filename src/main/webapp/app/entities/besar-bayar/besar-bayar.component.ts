import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBesarBayar } from 'app/shared/model/besar-bayar.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { BesarBayarService } from './besar-bayar.service';
import { BesarBayarDeleteDialogComponent } from './besar-bayar-delete-dialog.component';

@Component({
  selector: 'jhi-besar-bayar',
  templateUrl: './besar-bayar.component.html'
})
export class BesarBayarComponent implements OnInit, OnDestroy {
  isSaving = false;
  besarBayars: IBesarBayar[] | null = null;
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;
  create?: boolean;
  totalItems = 0;

  constructor(
    protected besarBayarService: BesarBayarService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.besarBayars = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.besarBayarService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IBesarBayar[]>) => this.onSuccess(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.besarBayars = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBesarBayars();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBesarBayar): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBesarBayars(): void {
    this.eventSubscriber = this.eventManager.subscribe('besarBayarListModification', () => this.reset());
  }

  delete(besarBayar: IBesarBayar): void {
    const modalRef = this.modalService.open(BesarBayarDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.besarBayar = besarBayar;
  }

  generate(id: number): void {
    this.subscribeToSaveResponse(this.besarBayarService.generate(id));
    console.log('id kelas:' + id);
    this.loadAll();
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  /*  protected paginateBesarBayars(data: IBesarBayar[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.besarBayars.push(data[i]);
      }
    }
  }*/

  private onSuccess(users: IBesarBayar[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    console.log(users);
    this.besarBayars = users;
  }

  previousState(): void {
    window.history.back();
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.loadAll();
  }

  protected onSaveError(): void {
    this.isSaving = false;
    this.loadAll();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBesarBayar>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
}
