import { Component } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITagihan } from 'app/shared/model/tagihan.model';
import { ITagihanDTO, TagihanDTO } from 'app/shared/model/tagihanDTO.model';
import { TagihanService } from './tagihan.service';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TransaksiService } from '../transaksi/transaksi.service';
import { ProsesTransferComponent } from './proses-transfer.component';
import { IPaymentChanel } from 'app/shared/model/payment-chanel.model';

@Component({
  templateUrl: './tagihan-detail-dialog.component.html',
  styleUrls: ['tagihan.scss']
})
export class TagihanDetailDialogComponent {
  isSaving = false;
  paymentChanel?: IPaymentChanel[] | null = null;
  tagihan?: ITagihan;
  searchOption!: string;
  tagihanAwal!: number;
  jmlBayar!: number;
  sisaTagihan!: number;
  idTagihan: number | undefined;

  editForm = this.fb.group({
    pOptions: 'TRANSFER',
    caraBayar: 'CASH',
    jmlBayar: [null, [Validators.required]],
    sisaTagihan: [{ value: null, disabled: true }],
    nominalPembayaran: [{ value: null, disabled: true }],
    jenisPembayaran: [{ value: null, disabled: true }],
    kelsaAjaran: [{ value: null, disabled: true }],
    harusBayar: [{ value: null, disabled: true }]
  });

  constructor(
    protected transaksiService: TransaksiService,
    protected tagihanService: TagihanService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder,
    protected modalService: NgbModal
  ) {}

  prosesTagihan(tagihan: ITagihan): void {
    this.editForm.patchValue({
      nominalPembayaran: tagihan.nominalPembayaran,
      jenisPembayaran: tagihan.jenisPembayaran,
      kelsaAjaran: tagihan.kelas + '-' + tagihan.tglTagihan,
      harusBayar: tagihan.harusBayar
    });
    this.idTagihan = tagihan.idTagihan;
  }

  save(): void {
    this.isSaving = true;
    const tagihan = this.createFromForm();
    this.subscribeToSaveResponse(this.transaksiService.createTransaksi(tagihan));
    this.eventManager.broadcast('tagihanListModification');
    this.activeModal.close();
  }

  transfer(va: string): void {
    this.isSaving = true;
    const tagihan = this.createFromForm();
    this.subscribeToSaveResponse(this.transaksiService.createTransaksiTransfer(tagihan, va));
    this.activeModal.close();
    this.eventManager.broadcast('tagihanListModification');
  }

  private createFromForm(): ITagihanDTO {
    console.log('id tagihan:' + this.idTagihan);
    return {
      ...new TagihanDTO(),
      jenisPembayaran: this.editForm.get(['jenisPembayaran'])!.value,
      nominalPembayaran: this.editForm.get(['nominalPembayaran'])!.value,
      harusBayar: this.editForm.get(['harusBayar'])!.value,
      metodeTransaksi: this.editForm.get(['pOptions'])!.value,
      caraBayar: this.editForm.get(['caraBayar'])!.value,
      jmlBayar: this.editForm.get(['jmlBayar'])!.value,
      jmlSisa: this.editForm.get(['sisaTagihan'])!.value,
      idTagihan: this.idTagihan
    };
  }

  clear(): void {
    this.activeModal.dismiss();
  }

  sisa(): void {
    this.jmlBayar = this.editForm.get(['jmlBayar'])!.value;
    this.tagihanAwal = this.editForm.get(['harusBayar'])!.value;
    this.sisaTagihan = this.tagihanAwal - this.jmlBayar;
    console.log('tagihan awal:' + this.tagihanAwal);
    console.log('jml bayar:' + this.jmlBayar);
    console.log('sisaTagihan:' + this.sisaTagihan);

    this.editForm.patchValue({
      sisaTagihan: this.sisaTagihan
    });
  }

  optionSearch(): void {
    this.searchOption = this.editForm.get(['pOptions'])!.value;
  }

  confirmDelete(id: number): void {
    this.tagihanService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tagihanListModification');
      this.activeModal.close();
    });
  }

  prosesTransfer(tagihan: ITagihan): void {
    const modalRef = this.modalService.open(ProsesTransferComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tagihan = tagihan;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITagihanDTO): any {
    return item.id;
  }
}
