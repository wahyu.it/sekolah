import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITagihan, Tagihan } from 'app/shared/model/tagihan.model';
import { TagihanService } from './tagihan.service';
import { TagihanComponent } from './tagihan.component';
import { TagihanDetailComponent } from './tagihan-detail.component';
import { TagihanUpdateComponent } from './tagihan-update.component';

@Injectable({ providedIn: 'root' })
export class TagihanResolve implements Resolve<ITagihan> {
  constructor(private service: TagihanService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITagihan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tagihan: HttpResponse<Tagihan>) => {
          if (tagihan.body) {
            return of(tagihan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Tagihan());
  }
}

export const tagihanRoute: Routes = [
  {
    path: '',
    component: TagihanComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Tagihan'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TagihanDetailComponent,
    resolve: {
      tagihan: TagihanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Tagihan'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TagihanUpdateComponent,
    resolve: {
      tagihan: TagihanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Tagihan'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TagihanUpdateComponent,
    resolve: {
      tagihan: TagihanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Tagihan'
    },
    canActivate: [UserRouteAccessService]
  }
];
