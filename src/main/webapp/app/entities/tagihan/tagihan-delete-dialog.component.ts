import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITagihan } from 'app/shared/model/tagihan.model';
import { TagihanService } from './tagihan.service';

@Component({
  templateUrl: './tagihan-delete-dialog.component.html'
})
export class TagihanDeleteDialogComponent {
  tagihan?: ITagihan;

  constructor(protected tagihanService: TagihanService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tagihanService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tagihanListModification');
      this.activeModal.close();
    });
  }
}
