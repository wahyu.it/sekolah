import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITagihan } from 'app/shared/model/tagihan.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TagihanService } from './tagihan.service';
import { TagihanDeleteDialogComponent } from './tagihan-delete-dialog.component';
import { FormBuilder } from '@angular/forms';
import { TagihanDetailDialogComponent } from './tagihan-detail-dialog.component';

@Component({
  selector: 'jhi-tagihan',
  templateUrl: './tagihan.component.html'
})
export class TagihanComponent implements OnInit, OnDestroy {
  isSaving = false;
  tagihans?: ITagihan[] | null = null;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  skey!: string;
  svalue!: string;
  previousPage: number | undefined;

  editForm = this.fb.group({
    pNis: [],
    pSiswa: [],
    pOptions: 1
  });

  constructor(
    protected tagihanService: TagihanService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  loadAll(): void {
    if (
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== null && this.svalue !== null)
    ) {
      console.log('data param');
      this.searchOption = 1;
      this.tagihanService
        .query({
          page: this.page - 1,
          skey: this.skey,
          svalue: this.svalue,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers));
      console.log('data tagihan:');
      console.log('this.skey: ' + this.skey);
      console.log('this.svalue: ' + this.svalue);
      console.log('this.page: ' + this.page);
    } else {
      console.log('data default');
      this.tagihanService
        .query({
          page: this.page - 1,
          skey: null,
          svalue: null,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers));
    }
  }

  loadPage(page?: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
      this.loadAll();
    }
  }

  previousState(): void {
    window.history.back();
  }

  searchByParam(paramName: string): void {
    this.skey = '';
    this.svalue = '';

    if (paramName === 'nis' && this.editForm.get(['pNis'])!.value !== null) {
      this.skey = 'nis';
      this.svalue = this.editForm.get(['pNis'])!.value;
    } else if (paramName === 'siswa' && this.editForm.get(['pSiswa'])!.value !== null) {
      this.skey = 'siswa';
      this.svalue = this.editForm.get(['pSiswa'])!.value;
    }

    console.log('searchByParameter by ' + this.skey + ': ' + this.svalue);

    if (
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== '' && this.svalue !== '') ||
      (this.skey !== null && this.svalue !== null)
    ) {
      console.log('data param');
      this.searchOption = 1;
      this.tagihanService
        .query({
          page: this.page - 1,
          skey: this.skey,
          svalue: this.svalue,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers));
      console.log('data tagihan:');
      console.log('this.skey: ' + this.skey);
      console.log('this.svalue: ' + this.svalue);
      console.log('this.page: ' + this.page);
    } else {
      console.log('data default');
      this.tagihanService
        .query({
          page: this.page - 1,
          skey: null,
          svalue: null,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers));
    }
  }

  optionSearch(): void {
    this.searchOption = this.editForm.get(['pOptions'])!.value;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      console.log('RUNNING DATA');
      console.log(data);
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.loadAll();
    });
    this.registerChangeInTagihans();
    this.skey = '';
    this.svalue = '';
  }

  clear(): void {
    this.editForm.patchValue({
      pNis: null,
      pSiswa: null
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITagihan): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTagihans(): void {
    this.eventSubscriber = this.eventManager.subscribe('tagihanListModification', () => this.loadAll());
  }

  delete(tagihan: ITagihan): void {
    const modalRef = this.modalService.open(TagihanDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tagihan = tagihan;
  }

  detailTransaksi(tagihan: ITagihan): void {
    const modalRef = this.modalService.open(TagihanDetailDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tagihan = tagihan;

    modalRef.componentInstance.prosesTagihan(tagihan);
    this.eventSubscriber = this.eventManager.subscribe('tagihanListModification', () => this.loadAll());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  private onSuccess(users: ITagihan[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.tagihans = users;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihan>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
}
