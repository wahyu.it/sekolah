import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ITagihan, Tagihan } from 'app/shared/model/tagihan.model';
import { TagihanService } from './tagihan.service';
import { IBesarBayar } from 'app/shared/model/besar-bayar.model';
import { BesarBayarService } from 'app/entities/besar-bayar/besar-bayar.service';

@Component({
  selector: 'jhi-tagihan-update',
  templateUrl: './tagihan-update.component.html'
})
export class TagihanUpdateComponent implements OnInit {
  isSaving = false;

  besarbayars: IBesarBayar[] = [];

  editForm = this.fb.group({
    id: [],
    nomor: [null, [Validators.required]],
    tanggal: [null, [Validators.required]],
    total: [null, [Validators.required]],
    jatuhTempo: [null, [Validators.required]],
    tanggalBayar: [],
    status: [],
    besarBayar: []
  });

  constructor(
    protected tagihanService: TagihanService,
    protected besarBayarService: BesarBayarService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tagihan }) => {
      this.updateForm(tagihan);

      this.besarBayarService
        .query({ filter: 'tagihan-is-null' })
        .pipe(
          map((res: HttpResponse<IBesarBayar[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IBesarBayar[]) => {
          if (!tagihan.besarBayar || !tagihan.besarBayar.id) {
            this.besarbayars = resBody;
          } else {
            this.besarBayarService
              .find(tagihan.besarBayar.id)
              .pipe(
                map((subRes: HttpResponse<IBesarBayar>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBesarBayar[]) => {
                this.besarbayars = concatRes;
              });
          }
        });
    });
  }

  updateForm(tagihan: ITagihan): void {
    this.editForm.patchValue({
      id: tagihan.id,
      nomor: tagihan.nomor,
      tanggal: tagihan.tanggal != null ? tagihan.tanggal.format(DATE_TIME_FORMAT) : null,
      total: tagihan.total,
      jatuhTempo: tagihan.jatuhTempo != null ? tagihan.jatuhTempo.format(DATE_TIME_FORMAT) : null,
      tanggalBayar: tagihan.tanggalBayar != null ? tagihan.tanggalBayar.format(DATE_TIME_FORMAT) : null,
      status: tagihan.status,
      besarBayar: tagihan.besarBayar
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tagihan = this.createFromForm();
    if (tagihan.id !== undefined) {
      this.subscribeToSaveResponse(this.tagihanService.update(tagihan));
    } else {
      this.subscribeToSaveResponse(this.tagihanService.create(tagihan));
    }
  }

  private createFromForm(): ITagihan {
    return {
      ...new Tagihan(),
      id: this.editForm.get(['id'])!.value,
      nomor: this.editForm.get(['nomor'])!.value,
      tanggal: this.editForm.get(['tanggal'])!.value != null ? moment(this.editForm.get(['tanggal'])!.value, DATE_TIME_FORMAT) : undefined,
      total: this.editForm.get(['total'])!.value,
      jatuhTempo:
        this.editForm.get(['jatuhTempo'])!.value != null ? moment(this.editForm.get(['jatuhTempo'])!.value, DATE_TIME_FORMAT) : undefined,
      tanggalBayar:
        this.editForm.get(['tanggalBayar'])!.value != null
          ? moment(this.editForm.get(['tanggalBayar'])!.value, DATE_TIME_FORMAT)
          : undefined,
      status: this.editForm.get(['status'])!.value,
      besarBayar: this.editForm.get(['besarBayar'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihan>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBesarBayar): any {
    return item.id;
  }
}
