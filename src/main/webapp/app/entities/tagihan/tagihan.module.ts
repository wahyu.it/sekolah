import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PondokSharedModule } from 'app/shared/shared.module';
import { TagihanComponent } from './tagihan.component';
import { TagihanDetailComponent } from './tagihan-detail.component';
import { TagihanUpdateComponent } from './tagihan-update.component';
import { TagihanDeleteDialogComponent } from './tagihan-delete-dialog.component';
import { tagihanRoute } from './tagihan.route';
import { TagihanDetailDialogComponent } from './tagihan-detail-dialog.component';
import { ProsesTransferComponent } from './proses-transfer.component';

@NgModule({
  imports: [PondokSharedModule, RouterModule.forChild(tagihanRoute)],
  declarations: [
    TagihanComponent,
    TagihanDetailComponent,
    TagihanUpdateComponent,
    TagihanDeleteDialogComponent,
    TagihanDetailDialogComponent,
    ProsesTransferComponent
  ],
  entryComponents: [TagihanDeleteDialogComponent, TagihanDetailDialogComponent, ProsesTransferComponent]
})
export class PondokTagihanModule {}
