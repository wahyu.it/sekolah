import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITagihan } from 'app/shared/model/tagihan.model';
import { IPaymentChanel } from 'app/shared/model/payment-chanel.model';

type EntityResponseType = HttpResponse<ITagihan>;
type EntityArrayResponseType = HttpResponse<ITagihan[]>;
type EntityArrayResponseTypePaymentChanel = HttpResponse<IPaymentChanel[]>;

@Injectable({ providedIn: 'root' })
export class TagihanService {
  public resourceUrl = SERVER_API_URL + 'api/tagihans';
  public resourceUrlTransaksi = SERVER_API_URL + 'api/transaksis/tagihan';
  public resourceUrlTransaksiManual = SERVER_API_URL + 'api/transaksis/add';
  public resourceUrlPc = SERVER_API_URL + 'api/payment_chanel';

  constructor(protected http: HttpClient) {}

  create(tagihan: ITagihan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tagihan);
    return this.http
      .post<ITagihan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tagihan: ITagihan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tagihan);
    return this.http
      .put<ITagihan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITagihan>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  createTransaksi2(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITagihan>(`${this.resourceUrlTransaksi}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  createTransaksi(tagihan: ITagihan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tagihan);
    return this.http
      .post<ITagihan>(this.resourceUrlTransaksiManual, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITagihan[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(tagihan: ITagihan): ITagihan {
    const copy: ITagihan = Object.assign({}, tagihan, {
      tanggal: tagihan.tanggal && tagihan.tanggal.isValid() ? tagihan.tanggal.toJSON() : undefined,
      jatuhTempo: tagihan.jatuhTempo && tagihan.jatuhTempo.isValid() ? tagihan.jatuhTempo.toJSON() : undefined,
      tanggalBayar: tagihan.tanggalBayar && tagihan.tanggalBayar.isValid() ? tagihan.tanggalBayar.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.tanggal = res.body.tanggal ? moment(res.body.tanggal) : undefined;
      res.body.jatuhTempo = res.body.jatuhTempo ? moment(res.body.jatuhTempo) : undefined;
      res.body.tanggalBayar = res.body.tanggalBayar ? moment(res.body.tanggalBayar) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tagihan: ITagihan) => {
        tagihan.tanggal = tagihan.tanggal ? moment(tagihan.tanggal) : undefined;
        tagihan.jatuhTempo = tagihan.jatuhTempo ? moment(tagihan.jatuhTempo) : undefined;
        tagihan.tanggalBayar = tagihan.tanggalBayar ? moment(tagihan.tanggalBayar) : undefined;
      });
    }
    return res;
  }
}
