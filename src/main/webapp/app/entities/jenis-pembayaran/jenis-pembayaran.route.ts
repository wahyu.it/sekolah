import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IJenisPembayaran, JenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { JenisPembayaranService } from './jenis-pembayaran.service';
import { JenisPembayaranComponent } from './jenis-pembayaran.component';
import { JenisPembayaranDetailComponent } from './jenis-pembayaran-detail.component';
import { JenisPembayaranUpdateComponent } from './jenis-pembayaran-update.component';

@Injectable({ providedIn: 'root' })
export class JenisPembayaranResolve implements Resolve<IJenisPembayaran> {
  constructor(private service: JenisPembayaranService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJenisPembayaran> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((jenisPembayaran: HttpResponse<JenisPembayaran>) => {
          if (jenisPembayaran.body) {
            return of(jenisPembayaran.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new JenisPembayaran());
  }
}

export const jenisPembayaranRoute: Routes = [
  {
    path: '',
    component: JenisPembayaranComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'JenisPembayarans'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: JenisPembayaranDetailComponent,
    resolve: {
      jenisPembayaran: JenisPembayaranResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'JenisPembayarans'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: JenisPembayaranUpdateComponent,
    resolve: {
      jenisPembayaran: JenisPembayaranResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'JenisPembayarans'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: JenisPembayaranUpdateComponent,
    resolve: {
      jenisPembayaran: JenisPembayaranResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'JenisPembayarans'
    },
    canActivate: [UserRouteAccessService]
  }
];
