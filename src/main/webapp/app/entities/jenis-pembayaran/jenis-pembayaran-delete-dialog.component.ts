import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { JenisPembayaranService } from './jenis-pembayaran.service';

@Component({
  templateUrl: './jenis-pembayaran-delete-dialog.component.html'
})
export class JenisPembayaranDeleteDialogComponent {
  jenisPembayaran?: IJenisPembayaran;

  constructor(
    protected jenisPembayaranService: JenisPembayaranService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.jenisPembayaranService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jenisPembayaranListModification');
      this.activeModal.close();
    });
  }
}
