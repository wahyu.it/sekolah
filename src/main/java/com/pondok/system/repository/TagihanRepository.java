package com.pondok.system.repository;

import com.pondok.system.domain.Tagihan;
import com.pondok.system.service.dto.TagihanDTO;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Tagihan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagihanRepository extends JpaRepository<Tagihan, Long> {
	
	@Query("SELECT new com.pondok.system.service.dto.TagihanDTO(s.nama, s.nis, k.nama, j.jenis, b.nominal, t.harusBayar, t.id, t.total, t.tanggal, t.jatuhTempo, t.tanggalBayar, t.status, j.metode)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id")
	Page<TagihanDTO> findAllTagihan(Pageable pageable);
	
	@Query("SELECT new com.pondok.system.service.dto.TagihanDTO(s.nama, s.nis, k.nama, j.jenis, b.nominal, t.harusBayar, t.id, t.total, t.tanggal, t.jatuhTempo, t.tanggalBayar, t.status, j.metode)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.id=:id")
	Page<TagihanDTO> findAllTagihanByLogin(
			@Param("id") Long id,
			Pageable pageable);
	
	@Query("SELECT new com.pondok.system.service.dto.TagihanDTO(s.nama, s.nis, k.nama, j.jenis, b.nominal, t.harusBayar, t.id, t.total, t.tanggal, t.jatuhTempo, t.tanggalBayar, t.status, j.metode)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.nis=:nis")
	Page<TagihanDTO> findTagihanByNis(
			@Param("nis") String nis,
			Pageable pageable);
	
	@Query("SELECT new com.pondok.system.service.dto.TagihanDTO(s.nama, s.nis, k.nama, j.jenis, b.nominal, t.harusBayar, t.id, t.total, t.tanggal, t.jatuhTempo, t.tanggalBayar, t.status, j.metode)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.nama LIKE %:nama%")
	Page<TagihanDTO> findTagihanByNama(
			@Param("nama") String nama,
			Pageable pageable);
	
	@Query("SELECT new com.pondok.system.service.dto.TagihanDTO(s.nama, s.nis, k.nama, j.jenis, b.nominal, g.harusBayar, g.id, g.total, g.tanggal, g.jatuhTempo, g.tanggalBayar, g.status, t.id, t.nomor, t.tanggal, t.metode, t.bank, t.nomorRekening, t.virtualAccount, t.status, t.updateBy, t.updateOn, t.jmlTagiha, t.jmlBayar, t.jmlSisa, t.caraBayar, j.metode)"
			+ " FROM Transaksi t"
			+ " INNER JOIN Tagihan g ON g.id = t.tagihan.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.nis=:nis")
	Page<TagihanDTO> findTransaksiByNis(
			@Param("nis") String nis,
			Pageable pageable);
	
	@Query("SELECT new com.pondok.system.service.dto.TagihanDTO(s.nama, s.nis, k.nama, j.jenis, b.nominal, g.harusBayar, g.id, g.total, g.tanggal, g.jatuhTempo, g.tanggalBayar, g.status, t.id, t.nomor, t.tanggal, t.metode, t.bank, t.nomorRekening, t.virtualAccount, t.status, t.updateBy, t.updateOn, t.jmlTagiha, t.jmlBayar, t.jmlSisa, t.caraBayar, j.metode)"
			+ " FROM Transaksi t"
			+ " INNER JOIN Tagihan g ON g.id = t.tagihan.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.nama LIKE %:nama%")
	Page<TagihanDTO> findTransaksiByNama(
			@Param("nama") String nama,
			Pageable pageable);
	
	@Query("SELECT new com.pondok.system.service.dto.TagihanDTO(s.nama, s.nis, k.nama, j.jenis, b.nominal, g.harusBayar, g.id, g.total, g.tanggal, g.jatuhTempo, g.tanggalBayar, g.status, t.id, t.nomor, t.tanggal, t.metode, t.bank, t.nomorRekening, t.virtualAccount, t.status, t.updateBy, t.updateOn, t.jmlTagiha, t.jmlBayar, t.jmlSisa, t.caraBayar, j.metode)"
			+ " FROM Transaksi t"
			+ " INNER JOIN Tagihan g ON g.id = t.tagihan.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id")
	Page<TagihanDTO> findAllTransaksi(Pageable pageable);

}
