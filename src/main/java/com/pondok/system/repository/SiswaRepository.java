package com.pondok.system.repository;

import com.pondok.system.domain.Kelas;
import com.pondok.system.domain.Siswa;
import com.pondok.system.domain.User;
import com.pondok.system.service.dto.ProfilDTO;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * Spring Data  repository for the Siswa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiswaRepository extends JpaRepository<Siswa, Long> {

	List<Siswa> findByKelasId(Long kelasId);

	Page<Siswa> findByKelasId(Long svalueKelas, Pageable pageable);

	Page<Siswa> findByKelasIdAndNis(Long kelasId, String svalue, Pageable pageable);

	Page<Siswa> findByKelasIdAndNama(Long kelasId, String svalue, Pageable pageable);

	Page<Siswa> findOneByNisAndKelas(String svalue, Long kelasId, Pageable pageable);

	List<Siswa> findByLoundry(boolean b);
	
	@Query("SELECT new com.pondok.system.service.dto.ProfilDTO(u.id, u.firstName, u.lastName, s.id, s.nis, s.nama, s.alamat, s.jenisKelamin, s.tempatLahir, s.tanggalLahir, s.waliMurid, s.noTelephone, s.tahunAjaran, s.status, k.nama)"
			+ " FROM Siswa s"
			+ " INNER JOIN User u ON u.id = s.user.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE u.id=:id")
	Optional<ProfilDTO> userProfil(
			@Param("id") Long id);

	Siswa findOneByUser(User user);

	Page<Siswa> findOneByNis(String svalue, Pageable pageable);
	
	@Query("SELECT s"
			+ " FROM Siswa s"
			+ " WHERE s.nama LIKE %:nama%")
	Page<Siswa> findByNama(
			@Param("nama") String nama,
			Pageable pageable);
	
	@Query("SELECT s"
			+ " FROM Siswa s"
			+ " WHERE s.nama LIKE %:nama% AND s.kelas.id=:kelasId")
	Page<Siswa> findByNamaAndKelas(
			@Param("nama") String nama,
			@Param("kelasId") Long kelasId,
			Pageable pageable);
	
    @Transactional
    @Modifying
    @Query("UPDATE Siswa " +
        " SET beasiswa =:beasiswa" +
        " WHERE id=:id")
    int updateBeasiswa(
        @Param("id") Long id,
        @Param("beasiswa") Boolean beasiswa);
	
    @Transactional
    @Modifying
    @Query("UPDATE Siswa " +
        " SET loundry =:loundry" +
        " WHERE id=:id")
    int updateLoundry(
        @Param("id") Long id,
        @Param("loundry") Boolean loundry);

}
