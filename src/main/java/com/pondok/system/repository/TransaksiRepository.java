package com.pondok.system.repository;

import com.pondok.system.domain.Transaksi;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Transaksi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransaksiRepository extends JpaRepository<Transaksi, Long> {

	Optional<Transaksi> findByTagihanId(Long id);
}
