package com.pondok.system.repository;

import com.pondok.system.domain.BesarBayar;
import com.pondok.system.domain.JenisPembayaran;
import com.pondok.system.service.dto.ProfilDTO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BesarBayar entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BesarBayarRepository extends JpaRepository<BesarBayar, Long> {

	List<BesarBayar> findByKelasId(Long kelasId);

	List<BesarBayar> findByJenisBayar(JenisPembayaran jp);

	List<BesarBayar> findByJenisBayarMetode(String string);

}
