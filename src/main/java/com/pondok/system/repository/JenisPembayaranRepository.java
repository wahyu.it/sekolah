package com.pondok.system.repository;

import com.pondok.system.domain.JenisPembayaran;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the JenisPembayaran entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JenisPembayaranRepository extends JpaRepository<JenisPembayaran, Long> {

	JenisPembayaran findByJenis(String string);

}
