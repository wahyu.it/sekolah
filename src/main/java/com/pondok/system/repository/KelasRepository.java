package com.pondok.system.repository;

import com.pondok.system.domain.Kelas;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Kelas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KelasRepository extends JpaRepository<Kelas, Long> {

}
