package com.pondok.system.web.rest;

import com.pondok.system.domain.BesarBayar;
import com.pondok.system.domain.JenisPembayaran;
import com.pondok.system.repository.BesarBayarRepository;
import com.pondok.system.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pondok.system.domain.BesarBayar}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BesarBayarResource {

    private final Logger log = LoggerFactory.getLogger(BesarBayarResource.class);

    private static final String ENTITY_NAME = "besarBayar";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BesarBayarRepository besarBayarRepository;

    public BesarBayarResource(BesarBayarRepository besarBayarRepository) {
        this.besarBayarRepository = besarBayarRepository;
    }

    /**
     * {@code POST  /besar-bayars} : Create a new besarBayar.
     *
     * @param besarBayar the besarBayar to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new besarBayar, or with status {@code 400 (Bad Request)} if the besarBayar has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/besar-bayars")
    public ResponseEntity<BesarBayar> createBesarBayar(@RequestBody BesarBayar besarBayar) throws URISyntaxException {
        log.debug("REST request to save BesarBayar : {}", besarBayar);
        if (besarBayar.getId() != null) {
            throw new BadRequestAlertException("A new besarBayar cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        if(besarBayar.getJenisBayar().getJenis().equals("SYAHRIAH") || besarBayar.getJenisBayar().getJenis().equals("LOUNDRY")) besarBayar.setStatus("DONE");
        	besarBayarRepository.save(besarBayar);
        
        return ResponseEntity.created(new URI("/api/besar-bayars/" + besarBayar.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, besarBayar.getId().toString()))
            .body(besarBayar);
    }

    /**
     * {@code PUT  /besar-bayars} : Updates an existing besarBayar.
     *
     * @param besarBayar the besarBayar to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated besarBayar,
     * or with status {@code 400 (Bad Request)} if the besarBayar is not valid,
     * or with status {@code 500 (Internal Server Error)} if the besarBayar couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/besar-bayars")
    public ResponseEntity<BesarBayar> updateBesarBayar(@RequestBody BesarBayar besarBayar) throws URISyntaxException {
        log.debug("REST request to update BesarBayar : {}", besarBayar);
        if (besarBayar.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BesarBayar result = besarBayarRepository.save(besarBayar);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, besarBayar.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /besar-bayars} : get all the besarBayars.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of besarBayars in body.
     */
    @GetMapping("/besar-bayars")
    public ResponseEntity<List<BesarBayar>> getAllBesarBayars(Pageable pageable) {
        log.debug("REST request to get a page of BesarBayars");
        Page<BesarBayar> page = besarBayarRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /besar-bayars/:id} : get the "id" besarBayar.
     *
     * @param id the id of the besarBayar to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the besarBayar, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/besar-bayars/{id}")
    public ResponseEntity<BesarBayar> getBesarBayar(@PathVariable Long id) {
        log.debug("REST request to get BesarBayar : {}", id);
        Optional<BesarBayar> besarBayar = besarBayarRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(besarBayar);
    }

    /**
     * {@code DELETE  /besar-bayars/:id} : delete the "id" besarBayar.
     *
     * @param id the id of the besarBayar to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/besar-bayars/{id}")
    public ResponseEntity<Void> deleteBesarBayar(@PathVariable Long id) {
        log.debug("REST request to delete BesarBayar : {}", id);
        besarBayarRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
