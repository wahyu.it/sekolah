package com.pondok.system.web.rest;

import com.pondok.system.domain.Authority;
import com.pondok.system.domain.Siswa;
import com.pondok.system.domain.Tagihan;
import com.pondok.system.domain.User;
import com.pondok.system.repository.SiswaRepository;
import com.pondok.system.repository.TagihanRepository;
import com.pondok.system.repository.UserRepository;
import com.pondok.system.security.AuthoritiesConstants;
import com.pondok.system.security.SecurityUtils;
import com.pondok.system.service.TagihanService;
import com.pondok.system.service.dto.TagihanDTO;
import com.pondok.system.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pondok.system.domain.Tagihan}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TagihanResource {

    private final Logger log = LoggerFactory.getLogger(TagihanResource.class);

    private static final String ENTITY_NAME = "tagihan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TagihanRepository tagihanRepository;

    private final TagihanService tagihanService;

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;

    public TagihanResource( SiswaRepository siswaRepository, TagihanRepository tagihanRepository, TagihanService tagihanService, UserRepository userRepository) {
        this.tagihanRepository = tagihanRepository;
        this.tagihanService = tagihanService;
        this.userRepository = userRepository;
        this.siswaRepository = siswaRepository;
    }

    /**
     * {@code POST  /tagihans} : Create a new tagihan.
     *
     * @param tagihan the tagihan to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tagihan, or with status {@code 400 (Bad Request)} if the tagihan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tagihans")
    public ResponseEntity<Tagihan> createTagihan(@Valid @RequestBody Tagihan tagihan) throws URISyntaxException {
        log.debug("REST request to save Tagihan : {}", tagihan);
        if (tagihan.getId() != null) {
            throw new BadRequestAlertException("A new tagihan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tagihan result = tagihanRepository.save(tagihan);
        return ResponseEntity.created(new URI("/api/tagihans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tagihans} : Updates an existing tagihan.
     *
     * @param tagihan the tagihan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tagihan,
     * or with status {@code 400 (Bad Request)} if the tagihan is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tagihan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tagihans")
    public ResponseEntity<Tagihan> updateTagihan(@Valid @RequestBody Tagihan tagihan) throws URISyntaxException {
        log.debug("REST request to update Tagihan : {}", tagihan);
        if (tagihan.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Tagihan result = tagihanRepository.save(tagihan);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tagihan.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tagihans} : get all the tagihans.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tagihans in body.
     */
    @GetMapping("/tagihans")
    public ResponseEntity<List<TagihanDTO>> getAllTagihans(
			@RequestParam(value = "skey", required = false) String skey,
			@RequestParam(value = "svalue", required = false) String svalue,
			Pageable pageable) {
        log.debug("REST request to get a page of Tagihans");
		
		String login = SecurityUtils.getCurrentUserLogin().get();
		Optional<User> user = userRepository.findOneByLogin(login);
		Siswa siswa = siswaRepository.findOneByUser(user.get());

		
		Page<TagihanDTO> page = null;
		if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.TU)) {
	        log.debug("login by TU");
	        if("nis".equals(skey) && svalue != null) {
	        	page = tagihanRepository.findTagihanByNis(svalue, pageable);
	        } else if ("siswa".equals(skey) && svalue != null) {
	        	page = tagihanRepository.findTagihanByNama(svalue, pageable);
	        } else if (("siswa".equals(skey) && svalue == null)||("nis".equals(skey) && svalue == null)){
	        	page = tagihanRepository.findAllTagihan( pageable);
	        } else {
	        	page = tagihanRepository.findAllTagihan( pageable);
	        }
		} else if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.WALIMURID)){
	        log.debug("login by wali murid");
	        page = tagihanRepository.findAllTagihanByLogin(siswa.getId(), pageable);
		} else {
	        log.debug("login by Admin");
			page = tagihanRepository.findAllTagihan( pageable);
		}
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tagihans/:id} : get the "id" tagihan.
     *
     * @param id the id of the tagihan to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tagihan, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tagihans/{id}")
    public ResponseEntity<Tagihan> getTagihan(@PathVariable Long id) {
        log.debug("REST request to get Tagihan : {}", id);
        Optional<Tagihan> tagihan = tagihanRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tagihan);
    }

    /**
     * {@code DELETE  /tagihans/:id} : delete the "id" tagihan.
     *
     * @param id the id of the tagihan to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tagihans/{id}")
    public ResponseEntity<Void> deleteTagihan(@PathVariable Long id) {
        log.debug("REST request to delete Tagihan : {}", id);
        tagihanRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
  
    @PostMapping("/tagihans/bulanan")
	public Tagihan tagihanBulanan(
			@RequestParam(value = "date", required = false) LocalDate date) throws URISyntaxException{
		log.debug("REST request to search Tagihan ..." );
		Tagihan tagihan = tagihanService.createMounthlyTagihan(date);
    	
        System.out.println(ResponseEntity.ok().body(tagihan));
		return tagihan;
    }
    
    @PutMapping("/tagihans/generatepartial/{id}")
	public ResponseEntity<Integer> generateTagihanPartial(@PathVariable Long id) throws URISyntaxException{
		log.debug("REST request to search Tagihan ..." );
    	
		int tagihan = tagihanService.createTagihanPartial(id);

        return new ResponseEntity<Integer>(tagihan, HttpStatus.OK);
    }
}
