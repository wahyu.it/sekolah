package com.pondok.system.web.rest;

import com.pondok.system.domain.Kelas;
import com.pondok.system.repository.KelasRepository;
import com.pondok.system.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pondok.system.domain.Kelas}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class KelasResource {

    private final Logger log = LoggerFactory.getLogger(KelasResource.class);

    private static final String ENTITY_NAME = "kelas";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KelasRepository kelasRepository;

    public KelasResource(KelasRepository kelasRepository) {
        this.kelasRepository = kelasRepository;
    }

    /**
     * {@code POST  /kelas} : Create a new kelas.
     *
     * @param kelas the kelas to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kelas, or with status {@code 400 (Bad Request)} if the kelas has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kelas")
    public ResponseEntity<Kelas> createKelas(@Valid @RequestBody Kelas kelas) throws URISyntaxException {
        log.debug("REST request to save Kelas : {}", kelas);
        if (kelas.getId() != null) {
            throw new BadRequestAlertException("A new kelas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Kelas result = kelasRepository.save(kelas);
        return ResponseEntity.created(new URI("/api/kelas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kelas} : Updates an existing kelas.
     *
     * @param kelas the kelas to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kelas,
     * or with status {@code 400 (Bad Request)} if the kelas is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kelas couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kelas")
    public ResponseEntity<Kelas> updateKelas(@Valid @RequestBody Kelas kelas) throws URISyntaxException {
        log.debug("REST request to update Kelas : {}", kelas);
        if (kelas.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Kelas result = kelasRepository.save(kelas);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, kelas.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /kelas} : get all the kelas.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kelas in body.
     */
    @GetMapping("/kelas")
    public ResponseEntity<List<Kelas>> getAllKelas(Pageable pageable) {
        log.debug("REST request to get a page of Kelas");
        Page<Kelas> page = kelasRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kelas/:id} : get the "id" kelas.
     *
     * @param id the id of the kelas to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kelas, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kelas/{id}")
    public ResponseEntity<Kelas> getKelas(@PathVariable Long id) {
        log.debug("REST request to get Kelas : {}", id);
        Optional<Kelas> kelas = kelasRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(kelas);
    }

    /**
     * {@code DELETE  /kelas/:id} : delete the "id" kelas.
     *
     * @param id the id of the kelas to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kelas/{id}")
    public ResponseEntity<Void> deleteKelas(@PathVariable Long id) {
        log.debug("REST request to delete Kelas : {}", id);
        kelasRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
