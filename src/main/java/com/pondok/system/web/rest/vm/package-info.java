/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pondok.system.web.rest.vm;
