package com.pondok.system.web.rest;

import com.pondok.system.domain.Siswa;
import com.pondok.system.domain.User;
import com.pondok.system.repository.SiswaRepository;
import com.pondok.system.repository.UserRepository;
import com.pondok.system.security.SecurityUtils;
import com.pondok.system.service.dto.ProfilDTO;
import com.pondok.system.service.dto.TagihanDTO;
import com.pondok.system.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pondok.system.domain.Siswa}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SiswaResource {

    private final Logger log = LoggerFactory.getLogger(SiswaResource.class);

    private static final String ENTITY_NAME = "siswa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SiswaRepository siswaRepository;

    private final UserRepository userRepository;

    public SiswaResource(SiswaRepository siswaRepository, UserRepository userRepository) {
        this.siswaRepository = siswaRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /siswas} : Create a new siswa.
     *
     * @param siswa the siswa to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new siswa, or with status {@code 400 (Bad Request)} if the siswa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/siswas")
    public ResponseEntity<Siswa> createSiswa(@Valid @RequestBody Siswa siswa) throws URISyntaxException {
        log.debug("REST request to save Siswa : {}", siswa);
        if (siswa.getId() != null) {
            throw new BadRequestAlertException("A new siswa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Siswa result = siswaRepository.save(siswa);
        return ResponseEntity.created(new URI("/api/siswas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /siswas} : Updates an existing siswa.
     *
     * @param siswa the siswa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated siswa,
     * or with status {@code 400 (Bad Request)} if the siswa is not valid,
     * or with status {@code 500 (Internal Server Error)} if the siswa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/siswas")
    public ResponseEntity<Siswa> updateSiswa(@Valid @RequestBody Siswa siswa) throws URISyntaxException {
        log.debug("REST request to update Siswa : {}", siswa);
        if (siswa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Siswa result = siswaRepository.save(siswa);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, siswa.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /siswas} : get all the siswas.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of siswas in body.
     */
    @GetMapping("/siswas")
    public ResponseEntity<List<Siswa>> getAllSiswas(
			@RequestParam(value = "skey", required = false) String skey,
			@RequestParam(value = "svalue", required = false) String svalue,
			@RequestParam(value = "svalueKelas", required = false) String svalueKelas,
			Pageable pageable) {
        log.debug("REST request to get a page of Siswas");
        
        Page<Siswa> page = null;
        if("nis".equals(skey) && svalue != null) {
            log.debug("find by nis");
        	page = siswaRepository.findOneByNis(svalue, pageable);
        } else if ("nis".equals(skey) && svalue != null && svalueKelas != null) {
        	Long kelasId = Long.valueOf(svalueKelas);
        	page = siswaRepository.findOneByNisAndKelas(svalue, kelasId, pageable);
        } else if ("siswa".equals(skey) && svalue != null) {
            log.debug("find by nama");
        	page = siswaRepository.findByNama(svalue, pageable);
        } else if ("siswa".equals(skey) && svalue != null && svalueKelas != null) {
            log.debug("find by nama");
        	Long kelasId = Long.valueOf(svalueKelas);
        	page = siswaRepository.findByNamaAndKelas(svalue, kelasId, pageable);
        } else if ("kelas".equals(skey) && svalueKelas != null) {
            log.debug("find by kelas");
        	Long kelasId = Long.valueOf(svalueKelas);
        	page = siswaRepository.findByKelasId(kelasId, pageable);
        } else if (("siswa".equals(skey) && svalue == null)||("nis".equals(skey) && svalue == null)){
            log.debug("find by null");
        	page = siswaRepository.findAll(pageable);
        } else {
        	log.debug("find by default");
        	page = siswaRepository.findAll(pageable);
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /siswas/:id} : get the "id" siswa.
     *
     * @param id the id of the siswa to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the siswa, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/siswas/{id}")
    public ResponseEntity<Siswa> getSiswa(@PathVariable Long id) {
        log.debug("REST request to get Siswa : {}", id);
        Optional<Siswa> siswa = siswaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(siswa);
    }

    /**
     * {@code DELETE  /siswas/:id} : delete the "id" siswa.
     *
     * @param id the id of the siswa to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/siswas/{id}")
    public ResponseEntity<Void> deleteSiswa(@PathVariable Long id) {
        log.debug("REST request to delete Siswa : {}", id);
        siswaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/siswas/profil")
	public Optional<ProfilDTO> absenHarianByLogin(){
		log.debug("REST request to siswa by Login..");
		
		String login = SecurityUtils.getCurrentUserLogin().get();
		log.debug("Login.."+ login);
		Optional<User> user = userRepository.findOneByLogin(login);
    	
		Optional<ProfilDTO> result = siswaRepository.userProfil(user.get().getId());
        return result;
    }
    
    @PutMapping("/siswas/beasiswa/{id}")
    public ResponseEntity<Integer> updateBeasiswa(@PathVariable Long id, @RequestBody(required = false) String tipe) {
        log.debug("REST request to update beasiswa : {} :{}", id, tipe);
        
        int result = 0;
        if (tipe.equals("true")) {
        	log.debug("tipe True");
            result = siswaRepository.updateBeasiswa(id, true);
        }else {
        	log.debug("tipe False");
        	result = siswaRepository.updateBeasiswa(id, false);
        }
        
        return new ResponseEntity<Integer>(result, HttpStatus.OK);
    }
    
    @PutMapping("/siswas/loundry/{id}")
    public ResponseEntity<Integer> updateLoundry(@PathVariable Long id, @RequestBody(required = false) String tipe) {
        log.debug("REST request to update beasiswa : {} :{}", id, tipe);
        
        int result = 0;
        if (tipe.equals("true")) {
        	log.debug("tipe True");
            result = siswaRepository.updateLoundry(id, true);
        }else {
        	log.debug("tipe False");
        	result = siswaRepository.updateLoundry(id, false);
        }
        
        return new ResponseEntity<Integer>(result, HttpStatus.OK);
    }
}
