package com.pondok.system.web.rest;

import com.pondok.system.domain.JenisPembayaran;
import com.pondok.system.domain.LaporanSpp;
import com.pondok.system.domain.Tagihan;
import com.pondok.system.domain.Transaksi;
import com.pondok.system.domain.User;
import com.pondok.system.domain.enumeration.MetodeTransaksi;
import com.pondok.system.domain.enumeration.StatusBayar;
import com.pondok.system.domain.enumeration.StatusTransaksi;
import com.pondok.system.repository.TagihanRepository;
import com.pondok.system.repository.TransaksiRepository;
import com.pondok.system.repository.UserRepository;
import com.pondok.system.security.SecurityUtils;
import com.pondok.system.service.dto.TagihanDTO;
import com.pondok.system.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

/**
 * REST controller for managing {@link com.pondok.system.domain.Transaksi}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TransaksiResource {

    private final Logger log = LoggerFactory.getLogger(TransaksiResource.class);

    private static final String ENTITY_NAME = "transaksi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TransaksiRepository transaksiRepository;

    private final UserRepository userRepository;

    private final TagihanRepository tagihanRepository;

    public TransaksiResource( TransaksiRepository transaksiRepository, UserRepository userRepository, TagihanRepository tagihanRepository) {
        this.transaksiRepository = transaksiRepository;
        this.userRepository = userRepository;
        this.tagihanRepository = tagihanRepository;
    }

    /**
     * {@code POST  /transaksis} : Create a new transaksi.
     *
     * @param transaksi the transaksi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transaksi, or with status {@code 400 (Bad Request)} if the transaksi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transaksis")
    public ResponseEntity<Transaksi> createTransaksi(@RequestBody Transaksi transaksi) throws URISyntaxException {
        log.debug("REST request to save Transaksi : {}", transaksi);
        if (transaksi.getId() != null) {
            throw new BadRequestAlertException("A new transaksi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Transaksi result = transaksiRepository.save(transaksi);
        return ResponseEntity.created(new URI("/api/transaksis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transaksis} : Updates an existing transaksi.
     *
     * @param transaksi the transaksi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transaksi,
     * or with status {@code 400 (Bad Request)} if the transaksi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transaksi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/transaksis")
    public ResponseEntity<Transaksi> updateTransaksi(@RequestBody Transaksi transaksi) throws URISyntaxException {
        log.debug("REST request to update Transaksi : {}", transaksi);
        if (transaksi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Transaksi result = transaksiRepository.save(transaksi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, transaksi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /transaksis} : get all the transaksis.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transaksis in body.
     */
    @GetMapping("/transaksis")
    public ResponseEntity<List<TagihanDTO>> getAllTransaksis(
			@RequestParam(value = "skey", required = false) String skey,
			@RequestParam(value = "svalue", required = false) String svalue,
			Pageable pageable) {
        log.debug("REST request to get a page of Transaksis");

		Page<TagihanDTO> page = null;
		
        if("nis".equals(skey) && svalue != null) {
            log.debug("cari nis:{}",svalue);
        	page = tagihanRepository.findTransaksiByNis(svalue, pageable);
        } else if ("siswa".equals(skey) && svalue != null) {
            log.debug("cari nama siswa:{}",svalue);
        	page = tagihanRepository.findTransaksiByNama(svalue, pageable);
        } else if (("siswa".equals(skey) && svalue == null)||("nis".equals(skey) && svalue == null)){
        	page = tagihanRepository.findAllTransaksi( pageable);
        } else {
        	page = tagihanRepository.findAllTransaksi( pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transaksis/:id} : get the "id" transaksi.
     *
     * @param id the id of the transaksi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transaksi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transaksis/{id}")
    public ResponseEntity<Transaksi> getTransaksi(@PathVariable Long id) {
        log.debug("REST request to get Transaksi : {}", id);
        Optional<Transaksi> transaksi = transaksiRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(transaksi);
    }

    /**
     * {@code DELETE  /transaksis/:id} : delete the "id" transaksi.
     *
     * @param id the id of the transaksi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transaksis/{id}")
    public ResponseEntity<Void> deleteTransaksi(@PathVariable Long id) {
        log.debug("REST request to delete Transaksi : {}", id);
        transaksiRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/transaksis/tagihan/{id}")
    public ResponseEntity<Transaksi> createTransaksiByTagihan(@PathVariable Long id) throws URISyntaxException {
    	log.debug("REST request to createTransaksiByTagihan : {}", id);
    	
    	Transaksi transaksi = transaksiRepository.findByTagihanId(id).get();
        if (transaksi.getId() != null) {
            throw new BadRequestAlertException("A new transaksi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        transaksi.setNomor("nomor_transaksi");// TODO
        transaksi.setTanggal(Instant.now());
    	log.debug("transaksi" + transaksi.getTagihan().getId());
        //TODO wait API
        
        Transaksi result = transaksiRepository.save(transaksi);
        return ResponseEntity.created(new URI("/api/transaksis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
    @PostMapping("/transaksis/add")
    public ResponseEntity<Transaksi> createTransaksiTagihan(@Valid @RequestBody TagihanDTO tagihanDTO) throws URISyntaxException {
        log.debug("REST request to save transaksi : {}", tagihanDTO);
        if (tagihanDTO.getIdTransaksi() != null) {
            throw new BadRequestAlertException("A new guestBook cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tagihan tagihan = tagihanRepository.findById(tagihanDTO.getIdTagihan()).get();
        
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
	        Transaksi tr = new Transaksi();
			        tr.setUpdateOn(Instant.now());
			        tr.setUpdateBy(user.getFirstName()+" "+user.getLastName());
			        tr.setNomor("nomor_transaksi");
			        tr.setJmlTagiha(tagihanDTO.getNominalPembayaran());
			        tr.setStatus(StatusTransaksi.SUKSES);
			        tr.setTanggal(Instant.now());
			        tr.setMetode(tagihanDTO.getMetodeTransaksi());
			        tr.setCaraBayar(tagihanDTO.getCaraBayar());
			        
			        if (!tagihan.getBesarBayar().getJenisBayar().getMetode().equals("ANGSURAN")) {
			            log.debug("non angusran");
			            log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			            tr.setJmlSisa(Long.valueOf(0));
			        	tagihan.setStatus(StatusBayar.LUNAS);
			            tr.setJmlBayar(tagihanDTO.getNominalPembayaran());
			            log.debug("jml sisa transaksi: {}", tr.getJmlSisa());
			        } else {
			            log.debug("angusran");
			            log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			            tr.setJmlSisa(tagihanDTO.getJmlSisa());
			            tr.setJmlBayar(tagihanDTO.getJmlBayar());
			            log.debug("jml sisa transaksi: {}", tr.getJmlSisa());
			            if ( tr.getJmlSisa() > 0) tagihan.setStatus(StatusBayar.ANGSUR);
			            if (tr.getJmlSisa() == 0) tagihan.setStatus(StatusBayar.LUNAS);
			        }
			        
	        tr.setTagihan(tagihan);
	        tagihan.setTanggalBayar(Instant.now());
	        tagihan.setHarusBayar(tr.getJmlSisa());
	        tagihanRepository.save(tagihan);
	        transaksiRepository.save(tr);
        
        return ResponseEntity.created(new URI("/api/guestBooks/" + tr.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, tr.getId().toString()))
            .body(tr);
    }
    
    @PostMapping("/transaksis/add/{va}")
    public ResponseEntity<Transaksi> createTransaksiTransfer(
    		@Valid @RequestBody TagihanDTO tagihanDTO,
    		@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to save transaksi : {}", tagihanDTO.getIdTagihan(), va);
        if (tagihanDTO.getIdTransaksi() != null) {
            throw new BadRequestAlertException("A new guestBook cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tagihan tagihan = tagihanRepository.findById(tagihanDTO.getIdTagihan()).get();
        
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
        Transaksi tr = new Transaksi();
        tr.setUpdateOn(Instant.now());
        tr.setUpdateBy(user.getFirstName()+" "+user.getLastName());
        tr.setNomor("nomor_transaksi");
        tr.setJmlTagiha(tagihanDTO.getNominalPembayaran());
        tr.setJmlBayar(tagihanDTO.getJmlBayar());
        tr.setJmlSisa(tagihanDTO.getJmlSisa());
        tr.setStatus(StatusTransaksi.PROSES);
        tr.setTanggal(Instant.now());
        tr.setMetode(MetodeTransaksi.TRANSFER);
        tr.setCaraBayar(tagihanDTO.getCaraBayar());
        tr.setBank(va);
        
        // TO DO post transfer
        

        if (!tagihan.getBesarBayar().getJenisBayar().getMetode().equals("ANGSURAN")) {
            log.debug("non angsuran");
            log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
            tr.setJmlSisa(Long.valueOf(0));
        	tagihan.setStatus(StatusBayar.MENUNGGU);
            tr.setJmlBayar(tagihanDTO.getNominalPembayaran());
            log.debug("jml sisa transaksi: {}", tr.getJmlSisa());
        } else {
            log.debug("angusran");
            log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
            tr.setJmlSisa(tagihanDTO.getJmlSisa());
            tr.setJmlBayar(tagihanDTO.getJmlBayar());
            log.debug("jml sisa transaksi: {}", tr.getJmlSisa());
            if ( tr.getJmlSisa() > 0) tagihan.setStatus(StatusBayar.ANGSUR);
            if (tr.getJmlSisa() == 0) tagihan.setStatus(StatusBayar.LUNAS);
        }
        
        tagihan.setTanggalBayar(Instant.now());
        tagihan.setHarusBayar(tr.getJmlSisa());
        tagihanRepository.save(tagihan);
        tr.setTagihan(tagihan);
        transaksiRepository.save(tr);
        
        return ResponseEntity.created(new URI("/api/guestBooks/" + tr.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, tr.getId().toString()))
            .body(tr);
    }
}
