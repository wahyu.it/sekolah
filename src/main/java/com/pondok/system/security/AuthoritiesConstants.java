package com.pondok.system.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String TU = "ROLE_TU";

    public static final String WALIMURID = "ROLE_WALI";

    public static final String WALIKELAS = "ROLE_KELAS";

    private AuthoritiesConstants() {
    }
}
