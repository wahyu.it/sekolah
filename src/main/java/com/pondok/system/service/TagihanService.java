package com.pondok.system.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pondok.system.domain.BesarBayar;
import com.pondok.system.domain.Siswa;
import com.pondok.system.domain.Tagihan;
import com.pondok.system.domain.enumeration.JenisBeasiswa;
import com.pondok.system.domain.enumeration.StatusBayar;
import com.pondok.system.repository.BesarBayarRepository;
import com.pondok.system.repository.JenisPembayaranRepository;
import com.pondok.system.repository.KelasRepository;
import com.pondok.system.repository.SiswaRepository;
import com.pondok.system.repository.TagihanRepository;

@Service
@Transactional
public class TagihanService {

    private final Logger log = LoggerFactory.getLogger(TagihanService.class);

    private final BesarBayarRepository besarBayarRepository;

    private final SiswaRepository siswaRepository;

    private final TagihanRepository tagihanRepository;

    private final KelasRepository kelasRepository;

    private final JenisPembayaranRepository jenisPembayaranRepository;
    
    public TagihanService(KelasRepository kelasRepository, BesarBayarRepository besarBayarRepository, SiswaRepository siswaRepository, TagihanRepository tagihanRepository, JenisPembayaranRepository jenisPembayaranRepository) {
    	this.besarBayarRepository = besarBayarRepository;
    	this.siswaRepository = siswaRepository;
    	this.tagihanRepository = tagihanRepository;
    	this.kelasRepository = kelasRepository;
    	this.jenisPembayaranRepository = jenisPembayaranRepository;
    }
	
    public Tagihan createMounthlyTagihan(LocalDate jobDate) {
    	
    	List<Siswa> listSiswa = siswaRepository.findAll();
    	log.debug("list siswa {}", listSiswa.size());
    	
    	LocalDate bulan = LocalDate.now();
    	
    	List<BesarBayar> listBesarBayar = besarBayarRepository.findByJenisBayarMetode("BULANAN");
    	List<BesarBayar> listSemester = besarBayarRepository.findByJenisBayarMetode("SEMESTER");
    	
    	for (BesarBayar besarBayar : listBesarBayar) {
    		if (besarBayar.getJenisBayar().getMetode().equals("BULANAN")) {
    			if (besarBayar.getJenisBayar().getJenis().toUpperCase().equals("LOUNDRY")) {
    				for (Siswa siswa : listSiswa) {
    					if (siswa.getKelas() == besarBayar.getKelas()) {
	    					if (siswa.getLoundry() != null && siswa.getLoundry() == true) {
	    				    	log.debug("list siswa {}", siswa.getNama());
	    							Tagihan tagihan = new Tagihan();
	    			        			tagihan.setNomor("nomor_tagihan");
	    			        			tagihan.setTanggal(siswa.getTahunAjaran());
	    			        			tagihan.setTotal(besarBayar.getJenisBayar().getJenis() +"-"+ bulan.getMonth().plus(1));
	    			        			tagihan.setHarusBayar(besarBayar.getNominal());
	    			        			tagihan.setStatus(StatusBayar.BELUMBAYAR);
	    			        			tagihan.setSiswa(siswa);
	    			        			tagihan.setBesarBayar(besarBayar);
	    			        	   tagihanRepository.save(tagihan);
    						}
    					}
    				}
    			} 
    			
    			if (besarBayar.getJenisBayar().getJenis().toUpperCase().equals("SYAHRIAH")) {
    				for (Siswa siswa : listSiswa) {
    					if (siswa.getKelas() == besarBayar.getKelas()) {
    						Tagihan tagihan = new Tagihan();
    			       			tagihan.setNomor("nomor_tagihan");
    			        		tagihan.setTanggal(siswa.getTahunAjaran());
    			        		tagihan.setTotal(besarBayar.getJenisBayar().getJenis() +"-"+ bulan.getMonth().plus(1));
    			        		tagihan.setHarusBayar(besarBayar.getNominal());
    			        		if (siswa.getBeasiswa() != null && siswa.getBeasiswa() == true) {
	        			        	if (siswa.getJmlBulan() != null && siswa.getJmlBulan() != 0) {
	        			       			tagihan.setStatus(StatusBayar.BEASISWA);
	        			       			siswa.setJmlBulan(siswa.getJmlBulan()-1);
	        			       			if (siswa.getJmlBulan() == 0) siswa.setBeasiswa(false);
	        			       			siswaRepository.save(siswa);
	        			        	}
    			        		} else {
        			        		tagihan.setStatus(StatusBayar.BELUMBAYAR);
    			        		}
    			        		tagihan.setSiswa(siswa);
    			        		tagihan.setBesarBayar(besarBayar);
    			           tagihanRepository.save(tagihan);
    			        }
    				}
    			} 
    		}
		}
    	
    	
			
		return null;
    }
	
    public int createTagihanPartial(Long id) {
    	
    	BesarBayar besarBayar = besarBayarRepository.findById(id).get();
    	
    	List<Siswa> listSiswa = siswaRepository.findByKelasId(besarBayar.getKelas().getId());
    	
			for (Siswa siswa : listSiswa) {
				Tagihan tagihan = new Tagihan();
		       		tagihan.setNomor("nomor_tagihan");
		       		tagihan.setTanggal(siswa.getTahunAjaran());
		       		tagihan.setTotal(besarBayar.getJenisBayar().getJenis());
		       		tagihan.setHarusBayar(besarBayar.getNominal());
		       		tagihan.setStatus(StatusBayar.BELUMBAYAR);
		       		tagihan.setSiswa(siswa);
		       		tagihan.setBesarBayar(besarBayar);
		       		tagihanRepository.save(tagihan);
			}	
    	
		besarBayar.setStatus("DONE");
		besarBayarRepository.save(besarBayar);
    	
		return 0;
    }
}
