package com.pondok.system.service.dto;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import com.pondok.system.domain.enumeration.JenisKelamin;

public class ProfilDTO {

    private Long userId;

    private String firstName;

    private String lastName;
    
    private Long siswaId;
    
    private String nis;

    private String nama;

    private String alamat;

    private JenisKelamin jenisKelamin;

    private String tempatLahir;

    private Instant tanggalLahir;

    private String waliMurid;

    private String noTelephone;

    private String tahunAjaran;

    private String status;
    
    private String kelas;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Long siswaId) {
		this.siswaId = siswaId;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public JenisKelamin getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(JenisKelamin jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public Instant getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Instant tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getWaliMurid() {
		return waliMurid;
	}

	public void setWaliMurid(String waliMurid) {
		this.waliMurid = waliMurid;
	}

	public String getNoTelephone() {
		return noTelephone;
	}

	public void setNoTelephone(String noTelephone) {
		this.noTelephone = noTelephone;
	}

	public String getTahunAjaran() {
		return tahunAjaran;
	}

	public void setTahunAjaran(String tahunAjaran) {
		this.tahunAjaran = tahunAjaran;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public ProfilDTO(Long userId, String firstName, String lastName, Long siswaId, String nis, String nama, String alamat, JenisKelamin jenisKelamin, String tempatLahir,
			Instant tanggalLahir, String waliMurid, String noTelephone, String tahunAjaran, String status, String kelas) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.siswaId = siswaId;
		this.nis = nis;
		this.nama = nama;
		this.alamat = alamat;
		this.jenisKelamin = jenisKelamin;
		this.tempatLahir = tempatLahir;
		this.tanggalLahir = tanggalLahir;
		this.waliMurid = waliMurid;
		this.noTelephone = noTelephone;
		this.tahunAjaran = tahunAjaran;
		this.status = status;
		this.kelas = kelas;
	}

	@Override
	public String toString() {
		return "ProfilDTO [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", siswaId=" + siswaId + ", nis=" + nis + ", nama=" + nama + ", alamat="
				+ alamat + ", jenisKelamin=" + jenisKelamin + ", tempatLahir=" + tempatLahir + ", tanggalLahir=" + tanggalLahir + ", waliMurid=" + waliMurid + ", noTelephone="
				+ noTelephone + ", tahunAjaran=" + tahunAjaran + ", status=" + status + ", kelas=" + kelas + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfilDTO other = (ProfilDTO) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
}
