package com.pondok.system.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.pondok.system.config.bean.util.OnlinePaymentMapper;
import com.pondok.system.domain.Tagihan;
import com.pondok.system.domain.Transaksi;

@Configuration
@Service
//@PropertySource({"classpath:application-${spring.profiles.active}"})
public class OnlineService {

    private final Logger log = LoggerFactory.getLogger(OnlineService.class);

    private String urlCreatePaymentURL = "https://api-sandbox.mcpayment.id:9000" + "/va/transactions?payment=<payment_channel>";
 
    private String connectTimeout = "60000";

    private String readTimeout = "60000";

	public Transaksi createPayment(Transaksi transaksi, String sessionId) {
		
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		String postData = null;
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(urlCreatePaymentURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Origin", "https://fidusia.ahu.go.id");
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("Referer", "https://fidusia.ahu.go.id/pendaftaran.html");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
		    writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(postData);
		    writer.flush();
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb.append(line);
		        }
		        
		        reader.close();
		        
		        String responsePayload = sb.toString();
		        log.debug(responsePayload);
		        
				String[] respTokens = responsePayload.split("\\^");
				if("1".equals(respTokens[0])) {
//					sertifikat.setRegistrationId(respTokens[1]);
//					log.debug("register SUCCESS {} in {}ms", akta.getPpk().getNomor(), (endTimestamp-startTimestamp));
		        } else {
//					log.debug("register FAILED {} {} {}ms", akta.getPpk().getNomor(), responsePayload, (endTimestamp-startTimestamp));
//					log.debug(postData);
				}
		    } else {
//				log.debug("register FAILED {} {}-{} {}ms", akta.getPpk().getNomor(), responseCode, responseMessage, (endTimestamp-startTimestamp));
//				log.debug(postData);
			}
		} catch (Exception e) {
//        	log.error("Error on register " + akta.getPpk().getNomor(), e);
//			log.debug(postData);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
		
		return transaksi;
	}
	
	public Transaksi cancelPayment(Transaksi transaksi, String sessionId) {
		Tagihan tagihan = transaksi.getTagihan();
		
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		String postData = null;
		
		try {
			postData = new OnlinePaymentMapper(tagihan).toString();
		} catch (Exception e) {
			log.error("Error on mapping register payload", e);
		}
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(urlCreatePaymentURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("DELETE");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Origin", "https://fidusia.ahu.go.id");
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("Referer", "https://fidusia.ahu.go.id/pendaftaran.html");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
		    writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(postData);
		    writer.flush();
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb.append(line);
		        }
		        
		        reader.close();
		        // TO DO get payment_code and transaction_id
		        
		        
//		        String responsePayload = sb.toString();
//		        log.debug(responsePayload);
//		        
//				String[] respTokens = responsePayload.split("\\^");
//				if("1".equals(respTokens[0])) {
////					sertifikat.setRegistrationId(respTokens[1]);
////					log.debug("register SUCCESS {} in {}ms", akta.getPpk().getNomor(), (endTimestamp-startTimestamp));
//		        } else {
////					log.debug("register FAILED {} {} {}ms", akta.getPpk().getNomor(), responsePayload, (endTimestamp-startTimestamp));
////					log.debug(postData);
//				}
		    } else {
//				log.debug("register FAILED {} {}-{} {}ms", akta.getPpk().getNomor(), responseCode, responseMessage, (endTimestamp-startTimestamp));
//				log.debug(postData);
			}
		} catch (Exception e) {
//        	log.error("Error on register " + akta.getPpk().getNomor(), e);
//			log.debug(postData);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
		
		return null;
	}

	public TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}

}
