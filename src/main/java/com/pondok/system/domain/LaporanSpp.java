package com.pondok.system.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A .Laporan Spp
 */
@Entity
@Table(name = "laporan_spp")
public class LaporanSpp implements Serializable  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("laporanspp")
    private Siswa siswa;

    @Column(name = "tahun_ajaran", nullable = false)
    private String tahunAjaran;

    @Column(name = "spp_juli", nullable = false)
    private String sppJuli;

    @Column(name = "spp_augst", nullable = false)
    private String sppAugst;

    @Column(name = "spp_sept", nullable = false)
    private String sppSept;

    @Column(name = "spp_okto", nullable = false)
    private String sppOkto;

    @Column(name = "spp_nov", nullable = false)
    private String sppNov;

    @Column(name = "spp_des", nullable = false)
    private String sppDes;

    @Column(name = "spp_jan", nullable = false)
    private String sppJan;

    @Column(name = "spp_feb", nullable = false)
    private String sppFeb;

    @Column(name = "spp_mart", nullable = false)
    private String sppMart;

	@Column(name = "spp_april", nullable = false)
    private String sppApril;

    @Column(name = "spp_mei", nullable = false)
    private String sppMei;

    @Column(name = "spp_juni", nullable = false)
    private String sppJuni;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Siswa getSiswa() {
		return siswa;
	}

	public void setSiswa(Siswa siswa) {
		this.siswa = siswa;
	}

	public String getTahunAjaran() {
		return tahunAjaran;
	}

	public void setTahunAjaran(String tahunAjaran) {
		this.tahunAjaran = tahunAjaran;
	}

	public String getSppJuli() {
		return sppJuli;
	}

	public void setSppJuli(String sppJuli) {
		this.sppJuli = sppJuli;
	}

	public String getSppAugst() {
		return sppAugst;
	}

	public void setSppAugst(String sppAugst) {
		this.sppAugst = sppAugst;
	}

	public String getSppSept() {
		return sppSept;
	}

	public void setSppSept(String sppSept) {
		this.sppSept = sppSept;
	}

	public String getSppOkto() {
		return sppOkto;
	}

	public void setSppOkto(String sppOkto) {
		this.sppOkto = sppOkto;
	}

	public String getSppNov() {
		return sppNov;
	}

	public void setSppNov(String sppNov) {
		this.sppNov = sppNov;
	}

	public String getSppDes() {
		return sppDes;
	}

	public void setSppDes(String sppDes) {
		this.sppDes = sppDes;
	}

	public String getSppJan() {
		return sppJan;
	}

	public void setSppJan(String sppJan) {
		this.sppJan = sppJan;
	}

	public String getSppFeb() {
		return sppFeb;
	}

	public void setSppFeb(String sppFeb) {
		this.sppFeb = sppFeb;
	}

	public String getSppMart() {
		return sppMart;
	}

	public void setSppMart(String sppMart) {
		this.sppMart = sppMart;
	}

	public String getSppApril() {
		return sppApril;
	}

	public void setSppApril(String sppApril) {
		this.sppApril = sppApril;
	}

	public String getSppMei() {
		return sppMei;
	}

	public void setSppMei(String sppMei) {
		this.sppMei = sppMei;
	}

	public String getSppJuni() {
		return sppJuni;
	}

	public void setSppJuni(String sppJuni) {
		this.sppJuni = sppJuni;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Siswa)) {
            return false;
        }
        return id != null && id.equals(((LaporanSpp) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "LaporanSpp [id=" + id + ", siswa=" + siswa + ", tahunAjaran=" + tahunAjaran + ", sppJuli=" + sppJuli + ", sppAugst=" + sppAugst + ", sppSept=" + sppSept
				+ ", sppOkto=" + sppOkto + ", sppNov=" + sppNov + ", sppDes=" + sppDes + ", sppJan=" + sppJan + ", sppFeb=" + sppFeb + ", sppMart=" + sppMart + ", sppApril="
				+ sppApril + ", sppMei=" + sppMei + ", sppJuni=" + sppJuni + "]";
	}

}
