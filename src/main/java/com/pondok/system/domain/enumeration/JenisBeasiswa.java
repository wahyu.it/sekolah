package com.pondok.system.domain.enumeration;

/**
 * The StatusBayar enumeration.
 */
public enum JenisBeasiswa {
    PRESTASI, DITANGGUHKAN, DITANGGUNG
}
