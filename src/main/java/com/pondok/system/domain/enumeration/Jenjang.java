package com.pondok.system.domain.enumeration;

/**
 * The Jenjang enumeration.
 */
public enum Jenjang {
    MA, MTS, MI, TK
}
