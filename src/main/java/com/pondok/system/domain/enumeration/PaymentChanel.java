package com.pondok.system.domain.enumeration;

/**
 * The Bank enumeration.
 */
public enum PaymentChanel {
    BCA, BNI, MANDIRI
}
