package com.pondok.system.domain.enumeration;

/**
 * The StatusBayar enumeration.
 */
public enum StatusBayar {
    LUNAS, BELUMBAYAR, NUNGGAK, ANGSUR, BEASISWA, MENUNGGU
}
