package com.pondok.system.domain.enumeration;

/**
 * The StatusTransaksi enumeration.
 */
public enum StatusTransaksi {
    SUKSES, PROSES, GAGAL
}
