package com.pondok.system.domain.enumeration;

/**
 * The JenisKelamin enumeration.
 */
public enum JenisKelamin {
    LAKILAKI, PEREMPUAN
}
