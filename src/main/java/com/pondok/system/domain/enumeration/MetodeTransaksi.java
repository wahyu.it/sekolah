package com.pondok.system.domain.enumeration;

/**
 * The MetodeTransaksi enumeration.
 */
public enum MetodeTransaksi {
    TUNAI, TRANSFER
}
