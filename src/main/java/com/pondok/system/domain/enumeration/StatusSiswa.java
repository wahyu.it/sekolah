package com.pondok.system.domain.enumeration;

public enum StatusSiswa {
	AKTIF, KELUAR, LULUS
}
