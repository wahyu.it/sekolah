package com.pondok.system.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A BesarBayar.
 */
@Entity
@Table(name = "besar_bayar")
public class BesarBayar implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nominal")
    private Long nominal;

    @ManyToOne
    @JsonIgnoreProperties("besarBayars")
    private Kelas kelas;

    @ManyToOne
    @JsonIgnoreProperties("besarBayars")
    private JenisPembayaran jenisBayar;

    @Column(name = "status")
    private String status;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setId(Long id) {
        this.id = id;
    }

    public Long getNominal() {
        return nominal;
    }

    public BesarBayar nominal(Long nominal) {
        this.nominal = nominal;
        return this;
    }

    public void setNominal(Long nominal) {
        this.nominal = nominal;
    }

    public Kelas getKelas() {
        return kelas;
    }

    public BesarBayar kelas(Kelas kelas) {
        this.kelas = kelas;
        return this;
    }

    public void setKelas(Kelas kelas) {
        this.kelas = kelas;
    }

    public JenisPembayaran getJenisBayar() {
        return jenisBayar;
    }

    public BesarBayar jenisBayar(JenisPembayaran jenisPembayaran) {
        this.jenisBayar = jenisPembayaran;
        return this;
    }

    public void setJenisBayar(JenisPembayaran jenisPembayaran) {
        this.jenisBayar = jenisPembayaran;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BesarBayar)) {
            return false;
        }
        return id != null && id.equals(((BesarBayar) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "BesarBayar [id=" + id + ", nominal=" + nominal + ", status=" + status + "]";
	}

//	@Override
//	public String toString() {
//		return "BesarBayar [id=" + id + ", nominal=" + nominal + "]";
//	}
}
