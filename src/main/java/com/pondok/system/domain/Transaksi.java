package com.pondok.system.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

import com.pondok.system.domain.enumeration.MetodeTransaksi;

import com.pondok.system.domain.enumeration.PaymentChanel;
import com.pondok.system.domain.enumeration.CaraBayar;
import com.pondok.system.domain.enumeration.StatusTransaksi;

/**
 * A Transaksi.
 */
@Entity
@Table(name = "transaksi")
public class Transaksi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nomor")
    private String nomor;

    @Column(name = "tanggal")
    private Instant tanggal;

    @Enumerated(EnumType.STRING)
    @Column(name = "metode")
    private MetodeTransaksi metode;

    @Column(name = "bank")
    private String bank;

    @Column(name = "nomor_rekening")
    private String nomorRekening;

    @Column(name = "virtual_account")
    private String virtualAccount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusTransaksi status;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private Instant updateOn;

    @Column(name = "jml_tagihan")
    private Long jmlTagiha;

    @Column(name = "jml_bayar")
    private Long jmlBayar;

    @Column(name = "jml_sisa")
    private Long jmlSisa;

    @Enumerated(EnumType.STRING)
    @Column(name = "cara_bayar")
    private CaraBayar caraBayar;

    @ManyToOne
    @JsonIgnoreProperties("transaksis")
    private Tagihan tagihan;

    public Long getJmlTagiha() {
		return jmlTagiha;
	}

	public void setJmlTagiha(Long jmlTagiha) {
		this.jmlTagiha = jmlTagiha;
	}

	public Long getJmlBayar() {
		return jmlBayar;
	}

	public void setJmlBayar(Long jmlBayar) {
		this.jmlBayar = jmlBayar;
	}

	public Long getJmlSisa() {
		return jmlSisa;
	}

	public void setJmlSisa(Long jmlSisa) {
		this.jmlSisa = jmlSisa;
	}

	public CaraBayar getCaraBayar() {
		return caraBayar;
	}

	public void setCaraBayar(CaraBayar caraBayar) {
		this.caraBayar = caraBayar;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public Transaksi nomor(String nomor) {
        this.nomor = nomor;
        return this;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public Instant getTanggal() {
        return tanggal;
    }

    public Transaksi tanggal(Instant tanggal) {
        this.tanggal = tanggal;
        return this;
    }

    public void setTanggal(Instant tanggal) {
        this.tanggal = tanggal;
    }

    public MetodeTransaksi getMetode() {
        return metode;
    }

    public Transaksi metode(MetodeTransaksi metode) {
        this.metode = metode;
        return this;
    }

    public void setMetode(MetodeTransaksi metode) {
        this.metode = metode;
    }

    public String getBank() {
        return bank;
    }

    public Transaksi bank(String bank) {
        this.bank = bank;
        return this;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNomorRekening() {
        return nomorRekening;
    }

    public Transaksi nomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
        return this;
    }

    public void setNomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
    }

    public String getVirtualAccount() {
        return virtualAccount;
    }

    public Transaksi virtualAccount(String virtualAccount) {
        this.virtualAccount = virtualAccount;
        return this;
    }

    public void setVirtualAccount(String virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public StatusTransaksi getStatus() {
        return status;
    }

    public Transaksi status(StatusTransaksi status) {
        this.status = status;
        return this;
    }

    public void setStatus(StatusTransaksi status) {
        this.status = status;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public Transaksi updateBy(String updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Instant getUpdateOn() {
        return updateOn;
    }

    public Transaksi updateOn(Instant updateOn) {
        this.updateOn = updateOn;
        return this;
    }

    public void setUpdateOn(Instant updateOn) {
        this.updateOn = updateOn;
    }

    public Tagihan getTagihan() {
        return tagihan;
    }

    public Transaksi tagihan(Tagihan tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public void setTagihan(Tagihan tagihan) {
        this.tagihan = tagihan;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaksi)) {
            return false;
        }
        return id != null && id.equals(((Transaksi) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Transaksi{" +
            "id=" + getId() +
            ", nomor='" + getNomor() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            ", metode='" + getMetode() + "'" +
            ", bank='" + getBank() + "'" +
            ", nomorRekening='" + getNomorRekening() + "'" +
            ", virtualAccount='" + getVirtualAccount() + "'" +
            ", status='" + getStatus() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            "}";
    }
}
