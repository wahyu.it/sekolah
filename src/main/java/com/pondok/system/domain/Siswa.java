package com.pondok.system.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pondok.system.domain.enumeration.JenisKelamin;

/**
 * A Siswa.
 */
@Entity
@Table(name = "siswa")
public class Siswa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nis", nullable = false)
    private String nis;

    @NotNull
    @Column(name = "nama", nullable = false)
    private String nama;

    @NotNull
    @Column(name = "alamat", nullable = false)
    private String alamat;

    @Enumerated(EnumType.STRING)
    @Column(name = "jenis_kelamin")
    private JenisKelamin jenisKelamin;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Column(name = "tanggal_lahir")
    private Instant tanggalLahir;

    @Column(name = "wali_murid")
    private String waliMurid;

    @Column(name = "no_telephone")
    private String noTelephone;

    @Column(name = "tahun_ajaran")
    private String tahunAjaran;

    @Column(name = "status")
    private String status;

    @Column(name = "beasiswa")
    private Boolean beasiswa;

    @Column(name = "loundry")
    private Boolean loundry;

    @Column(name = "jenis_beasiswa")
    private String jenisBeasiswa;

    @Column(name = "jml_bulan")
    private Long jmlBulan;

    @ManyToOne
    @JsonIgnoreProperties("siswas")
    private Kelas kelas;

	public Kelas getKelas() {
		return kelas;
	}

	public void setKelas(Kelas kelas) {
		this.kelas = kelas;
	}

	@OneToOne
    @JoinColumn(unique = true)
    private User user;
    
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNis() {
        return nis;
    }

    public Siswa nis(String nis) {
        this.nis = nis;
        return this;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public String getNama() {
        return nama;
    }

    public Siswa nama(String nama) {
        this.nama = nama;
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public Siswa alamat(String alamat) {
        this.alamat = alamat;
        return this;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public JenisKelamin getJenisKelamin() {
        return jenisKelamin;
    }

    public Siswa jenisKelamin(JenisKelamin jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
        return this;
    }

    public void setJenisKelamin(JenisKelamin jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public Siswa tempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
        return this;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public Instant getTanggalLahir() {
        return tanggalLahir;
    }

    public Siswa tanggalLahir(Instant tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
        return this;
    }

    public void setTanggalLahir(Instant tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getWaliMurid() {
        return waliMurid;
    }

    public Siswa waliMurid(String waliMurid) {
        this.waliMurid = waliMurid;
        return this;
    }

    public void setWaliMurid(String waliMurid) {
        this.waliMurid = waliMurid;
    }

    public String getNoTelephone() {
        return noTelephone;
    }

    public Siswa noTelephone(String noTelephone) {
        this.noTelephone = noTelephone;
        return this;
    }

    public void setNoTelephone(String noTelephone) {
        this.noTelephone = noTelephone;
    }

    public String getTahunAjaran() {
        return tahunAjaran;
    }

    public Siswa tahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
        return this;
    }

    public void setTahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Boolean getBeasiswa() {
		return beasiswa;
	}

	public void setBeasiswa(Boolean beasiswa) {
		this.beasiswa = beasiswa;
	}

	public Boolean getLoundry() {
		return loundry;
	}

	public void setLoundy(Boolean loundry) {
		this.loundry = loundry;
	}

	public String getJenisBeasiswa() {
		return jenisBeasiswa;
	}

	public void setJenisBeasiswa(String jenisBeasiswa) {
		this.jenisBeasiswa = jenisBeasiswa;
	}

	public Long getJmlBulan() {
		return jmlBulan;
	}

	public void setJmlBulan(Long jmlBulan) {
		this.jmlBulan = jmlBulan;
	}

	public void setLoundry(Boolean loundry) {
		this.loundry = loundry;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Siswa)) {
            return false;
        }
        return id != null && id.equals(((Siswa) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Siswa [id=" + id + ", nis=" + nis + ", nama=" + nama + ", alamat=" + alamat + ", jenisKelamin=" + jenisKelamin + ", tempatLahir=" + tempatLahir
				+ ", tanggalLahir=" + tanggalLahir + ", waliMurid=" + waliMurid + ", noTelephone=" + noTelephone + ", tahunAjaran=" + tahunAjaran + ", status=" + status
				+ ", beasiswa=" + beasiswa + ", loundry=" + loundry + ", jenisBeasiswa=" + jenisBeasiswa + ", jmlBulan=" + jmlBulan + ", kelas=" + kelas + ", user=" + user
				+ "]";
	}

//    @Override
//    public String toString() {
//        return "Siswa{" +
//            "id=" + getId() +
//            ", nis='" + getNis() + "'" +
//            ", nama='" + getNama() + "'" +
//            ", alamat='" + getAlamat() + "'" +
//            ", jenisKelamin='" + getJenisKelamin() + "'" +
//            ", tempatLahir='" + getTempatLahir() + "'" +
//            ", tanggalLahir='" + getTanggalLahir() + "'" +
//            ", waliMurid='" + getWaliMurid() + "'" +
//            ", noTelephone='" + getNoTelephone() + "'" +
//            ", tahunAjaran='" + getTahunAjaran() + "'" +
//            "}";
//    }
}
