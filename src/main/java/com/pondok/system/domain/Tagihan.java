package com.pondok.system.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pondok.system.domain.enumeration.StatusBayar;

/**
 * A Tagihan.
 */
@Entity
@Table(name = "tagihan")
public class Tagihan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nomor", nullable = false)
    private String nomor;

    @Column(name = "tanggal", nullable = false)
    private String tanggal;

    @Column(name = "total", nullable = false)
    private String total;

    @Column(name = "harus_bayar", nullable = false)
    private Long harusBayar;

    @Column(name = "jatuh_tempo", nullable = false)
    private Instant jatuhTempo;

    @Column(name = "tanggal_bayar")
    private Instant tanggalBayar;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusBayar status;

    @OneToOne
    private BesarBayar besarBayar;

    @ManyToOne
    @JsonIgnoreProperties("tagihans")
    private Siswa siswa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public Siswa getSiswa() {
		return siswa;
	}

	public void setSiswa(Siswa siswa) {
		this.siswa = siswa;
	}

	public void setId(Long id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public Tagihan nomor(String nomor) {
        this.nomor = nomor;
        return this;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getTanggal() {
        return tanggal;
    }

    public Tagihan tanggal(String tanggal) {
        this.tanggal = tanggal;
        return this;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTotal() {
        return total;
    }

    public Tagihan total(String total) {
        this.total = total;
        return this;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Instant getJatuhTempo() {
        return jatuhTempo;
    }

    public Tagihan jatuhTempo(Instant jatuhTempo) {
        this.jatuhTempo = jatuhTempo;
        return this;
    }

    public void setJatuhTempo(Instant jatuhTempo) {
        this.jatuhTempo = jatuhTempo;
    }

    public Instant getTanggalBayar() {
        return tanggalBayar;
    }

    public Tagihan tanggalBayar(Instant tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
        return this;
    }

    public void setTanggalBayar(Instant tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
    }

    public StatusBayar getStatus() {
        return status;
    }

    public Tagihan status(StatusBayar status) {
        this.status = status;
        return this;
    }

    public void setStatus(StatusBayar status) {
        this.status = status;
    }

    public BesarBayar getBesarBayar() {
        return besarBayar;
    }

    public Tagihan besarBayar(BesarBayar besarBayar) {
        this.besarBayar = besarBayar;
        return this;
    }

    public void setBesarBayar(BesarBayar besarBayar) {
        this.besarBayar = besarBayar;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Long getHarusBayar() {
		return harusBayar;
	}

	public void setHarusBayar(Long harusBayar) {
		this.harusBayar = harusBayar;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tagihan)) {
            return false;
        }
        return id != null && id.equals(((Tagihan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Tagihan{" +
            "id=" + getId() +
            ", nomor='" + getNomor() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            ", total=" + getTotal() +
            ", jatuhTempo='" + getJatuhTempo() + "'" +
            ", tanggalBayar='" + getTanggalBayar() + "'" +
            ", status='" + getStatus() + "'" +
            ", siswa='" + getSiswa() + "'" +
            ", harusBayar='" + getHarusBayar() + "'" +
            "}";
    }
}
