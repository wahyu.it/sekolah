package com.pondok.system.config;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.pondok.system.service.TagihanService;


@Configuration
@EnableScheduling
public class TagihanBulananSchedulerConfig {

	private static final Logger log = LoggerFactory.getLogger(TagihanBulananSchedulerConfig.class);
	
	private final TagihanService tagihanService;
	
	public TagihanBulananSchedulerConfig(TagihanService tagihanService) {
		this.tagihanService = tagihanService;
	}

	@Scheduled(cron = "0 0 23 27 7/1 *")//0 30 18 ? * SUN
    public void perform() throws Exception {

		try {
			log.info("start create tagihan bulanan.....");
			LocalDate jobDate = LocalDate.now();
			tagihanService.createMounthlyTagihan(jobDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

}
